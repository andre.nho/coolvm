#ifndef GLOBAL_HH_
#define GLOBAL_HH_

#include <cstdint>

struct G {
    static class CoolVM*          coolvm();
    static class MainWindow*      mw();
    static class QFont*           monospace();
    static class ResourceManager* resources();
};

// TODO - put this somewhere else?
void long_hex(char buf[20], uint32_t value);

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
