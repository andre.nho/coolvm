#ifndef VIDEOTAB_HH_
#define VIDEOTAB_HH_

#include <QWidget>

#include "tiletable.hh"
#include "updatable.hh"
#include "palette.hh"

class VideoTab : public QWidget, IUpdatable {
    Q_OBJECT
public:
    VideoTab();

    void Update();

private:
    Palette               _palette;
    class PaletteView*    _palette_view;
    class BackgroundView* _background_view;
    class TileTable*      _tileset;
    class TileMap*        _tilemap;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
