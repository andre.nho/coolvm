#include "tiletable.hh"

#include <QAbstractItemView>
#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QGridLayout>
#include <QHeaderView>
#include <QTableView>

#include "coolvm.hh"
#include "global.hh"
#include "resourcemanager.hh"

// {{{ MODEL

QVariant
TileTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        char buf[20];
        if (orientation == Qt::Vertical) {
            sprintf(buf, "_%X", section * 2);
        } else {
            sprintf(buf, "%X_", section);
        }
        return buf;
    }
    return QVariant();
}


void
TileTableModel::UpdateZoom(QModelIndex const& idx)
{
    auto pixmap = data(idx, Qt::DecorationRole).value<QPixmap>();

    QGraphicsScene* scene = new QGraphicsScene();
    _zoom->setScene(scene);
    QGraphicsPixmapItem* item = new QGraphicsPixmapItem(pixmap.scaled(128, 128));
    scene->addItem(item);
    _zoom->show();

    current_idx = idx;
}


void
TileTableModel::Update()
{
    emit dataChanged(index(0, 0), index(rowCount(), columnCount()));
    UpdateZoom(current_idx);
}


QVariant
TileSetModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DecorationRole) {
        uint16_t idx = (index.row() * 256) + index.column();
        return G::resources()->TilePixmap(idx).scaled(32, 32);
    }

    return QVariant();
}

// }}}

TileTable::TileTable(TileTableModel* model)
    : Table(new QTableView), Zoom(new QGraphicsView), _model(model)
{
    // widget config
    Zoom->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    Zoom->setMaximumSize(130, 130);

    _model->SetZoom(Zoom);
    
    Table->setModel(model);
    Table->verticalHeader()->setDefaultSectionSize(20);
    for (int i=0; i < 16; ++i) {
        Table->setColumnWidth(i, 33);
    }
    QHeaderView *verticalHeader = Table->verticalHeader();
    verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
    verticalHeader->setDefaultSectionSize(33);
    Table->setStyleSheet("QTableView::item {border: 0px; padding: -3px;} ");

    // events
    connect(Table, &QAbstractItemView::clicked, [&](const QModelIndex &index) { 
        _model->UpdateZoom(index); 
    });

    // layout
    auto layout = new QGridLayout;
    layout->addWidget(Table, 0, 0, 2, 1);
    layout->addWidget(Zoom, 0, 1);
    setLayout(layout);
}


void
TileTable::Update()
{
    _model->Update();
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
