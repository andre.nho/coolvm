#ifndef TILETABLE_HH_
#define TILETABLE_HH_

#include <QAbstractTableModel>
#include <QItemSelection>
#include <QWidget>

#include "updatable.hh"

class TileTableModel : public QAbstractTableModel {
    Q_OBJECT
public:
    virtual void  Update();
    void          SetZoom(class QGraphicsView* zoom) { _zoom = zoom; }
    void          UpdateZoom(QModelIndex const& idx);
protected:
    int           rowCount(const QModelIndex& = QModelIndex()) const { return 128; }
    int           columnCount(const QModelIndex& = QModelIndex()) const { return 16; }
    Qt::ItemFlags flags(const QModelIndex&) const { return Qt::ItemIsSelectable | Qt::ItemIsEnabled; }
    QVariant      headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    void          selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    QModelIndex current_idx;
    class QGraphicsView* _zoom;
};


class TileSetModel : public TileTableModel {
protected:
    QVariant data(const QModelIndex &index, int role) const;
};


class TileTable : public QWidget, IUpdatable {
    Q_OBJECT
public:
    TileTable(TileTableModel* model);

    void Update();

    class QTableView*    Table;
    class QGraphicsView* Zoom;

private:
    TileTableModel*      _model;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
