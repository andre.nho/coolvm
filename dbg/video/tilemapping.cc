#include "video/tilemapping.hh"

#include <QGraphicsView>
#include <QGridLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QMouseEvent>
#include <QTableView>
#include <QVBoxLayout>
#include <QLabel>

#include "coolvm.hh"
#include "global.hh"
#include "mainwindow.hh"
#include "spinbox.hh"

// {{{ ABSTRACT MAP

AbstractMap::AbstractMap(bool displaceable, AbstractMapModel* map_model)
    : _displaceable(displaceable),
      _map_model(map_model),
      _size_x(new SpinBox), _size_y(new SpinBox),
      _tile_number(new HexSpinBox("0x%03X")),
      _view(new QTableView), 
      _zoom(new QGraphicsView),
      _selected_tile(new QLabel(tr("No tile selected"))),
      _gr1(new QGroupBox(tr("Configuration"))), _gr2(new QGroupBox("REPLACE ME"))
{
    // widgets configuration
    _view->setModel(_map_model);
    _view->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    _view->setFixedSize(Video::WIDTH * 2, Video::HEIGHT * 2);

    _zoom->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    _zoom->setFixedSize(66, 66);
    _size_x->setRange(-15, 15);
    _size_y->setRange(-15, 15);
    _tile_number->setRange(0, 0x7FF);
    _tile_number->setEnabled(false);

    _view->verticalHeader()->setDefaultSectionSize(20);
    for (int i=0; i < _view->model()->columnCount(); ++i) {
        _view->setColumnWidth(i, 16);
    }
    QHeaderView *verticalHeader = _view->verticalHeader();
    verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
    verticalHeader->setDefaultSectionSize(16);
    verticalHeader->hide();
    _view->horizontalHeader()->hide();
    _view->setStyleSheet("QTableView::item {border: 0px; padding: -3px;} ");

    // events
    auto size_changed = [&]() {
        SetSize(_size_x->value(), _size_y->value());
        G::mw()->Update();
    };
    connect(_size_x, qOverload<int>(&QSpinBox::valueChanged), size_changed);
    connect(_size_y, qOverload<int>(&QSpinBox::valueChanged), size_changed);

    // layout
    CreateLayout();
}


void
AbstractMap::CreateLayout()
{
    auto wmain = new QWidget;

    QVBoxLayout *v = new QVBoxLayout;
    QHBoxLayout *h = new QHBoxLayout;
    
    // configuration
    auto g = new QGridLayout;
    g->addWidget(new QLabel(tr("Tile size x")), 0, 0);
    g->addWidget(_size_x, 0, 1);
    g->addWidget(new QLabel(tr("Tile size y")), 0, 2);
    g->addWidget(_size_y, 0, 3);
    _gr1->setLayout(g);
    _gr1->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    h->addWidget(_gr1);

    // detail
    auto gr3 = new QGroupBox(tr("Detail"));
    g = new QGridLayout;
    g->addWidget(_zoom, 0, 0, 3, 1);
    g->addWidget(_selected_tile, 0, 1);
    g->addWidget(_tile_number, 1, 1);
    _tile_number->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    gr3->setLayout(g);
    h->addWidget(gr3);
    h->addStretch();

    v->addLayout(h);

    // tilemap
    auto x = new QVBoxLayout;
    x->addWidget(_view);
    _gr2->setLayout(x);
    v->addWidget(_gr2);

    v->addStretch();
    wmain->setLayout(v);
    wmain->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    setWidget(wmain);
}

void 
AbstractMap::Update()
{
    int8_t x, y;
    Size(x, y);
    _size_x->setValue(x);
    _size_y->setValue(y);

    for (int i=0; i < _view->model()->columnCount(); ++i) {
        _view->setColumnWidth(i, 16+x);
    }
    _view->verticalHeader()->setDefaultSectionSize(16+y);
}

// }}}

// {{{ TILEMAP
    
TileMap::TileMap() 
    : AbstractMap(true, new TileMapModel),
      _pos_x(new SpinBox), _pos_y(new SpinBox)
{
    _view->setFixedSize(Video::WIDTH * 2 + 2, Video::HEIGHT * 2 + 2);

    auto g = qobject_cast<QGridLayout*>(_gr1->layout());
    g->addWidget(new QLabel(tr("Tile position x")), 1, 0);
    g->addWidget(_pos_x, 1, 1);
    g->addWidget(new QLabel(tr("Tile position y")), 1, 2);
    g->addWidget(_pos_y, 1, 3);

    _pos_x->setRange(0, Video::WIDTH);
    _pos_y->setRange(0, Video::HEIGHT);
    
    _gr2->setTitle(tr("Tilemap"));
}

void
TileMap::Update()
{
    AbstractMap::Update();

    uint16_t x, y;
    Displacement(x, y);
    _pos_x->setValue(x);
    _pos_y->setValue(y);
}

void    TileMap::Displacement(uint16_t& x, uint16_t& y) const { G::coolvm()->video.TilesetDisplacement(x, y); }
void    TileMap::SetDisplacement(uint16_t x, uint16_t y) { G::coolvm()->video.SetTilesetDisplacement(x, y); }
void    TileMap::Size(int8_t& x, int8_t& y) const { G::coolvm()->video.TileSize(x, y); } 
void    TileMap::SetSize(int8_t x, int8_t y) { G::coolvm()->video.SetTileSize(x, y); } 

uint8_t TileMapModel::Tile(uint16_t n) const { return G::coolvm()->video.TileTile(n); }
void    TileMapModel::SetTile(uint16_t n, uint8_t data) { G::coolvm()->video.SetTileTile(n, data); }

// }}}

// {{{ MODEL

QVariant
AbstractMapModel::data(const QModelIndex &index, int role) const
{
    return QVariant();
}


QVariant
AbstractMapModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    return QVariant();
}


void
AbstractMapModel::Update()
{
}

// }}}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
