#ifndef TILEMAPPING_HH_
#define TILEMAPPING_HH_

#include <QAbstractTableModel>
#include <QGraphicsScene>
#include <QScrollArea>

#include "updatable.hh"

class AbstractMapModel : public QAbstractTableModel, IUpdatable {
public:
    void Update();
protected:
    Qt::ItemFlags flags(const QModelIndex&) const { return Qt::ItemIsSelectable | Qt::ItemIsEnabled; }
    QVariant      data(const QModelIndex &index, int role) const;
    QVariant      headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    virtual uint8_t Tile(uint16_t n) const = 0;
    virtual void    SetTile(uint16_t n, uint8_t data) = 0;
};


class AbstractMap : public QScrollArea, IUpdatable {
public:
    AbstractMap(bool displaceable, AbstractMapModel* map_model);
    virtual void Update();

protected:
    virtual void Displacement(uint16_t& x, uint16_t& y) const = 0;
    virtual void SetDisplacement(uint16_t x, uint16_t y) = 0;
    virtual void Size(int8_t& x, int8_t& y) const = 0;
    virtual void SetSize(int8_t x, int8_t y) = 0;

    virtual void CreateLayout();

    bool _displaceable;
    AbstractMapModel    *_map_model;
    class SpinBox       *_size_x, *_size_y;
    class HexSpinBox    *_tile_number;
    class QTableView    *_view;
    class QGraphicsView *_zoom;
    class QLabel        *_selected_tile;
    class QGroupBox     *_gr1, *_gr2;
};

// --- implementations ---

class TileMap : public AbstractMap {
public:
    TileMap();
    void Update();

protected:
    void    Displacement(uint16_t& x, uint16_t& y) const;
    void    SetDisplacement(uint16_t x, uint16_t y);
    void    Size(int8_t& x, int8_t& y) const;
    void    SetSize(int8_t x, int8_t y);

private:
    class SpinBox *_pos_x, *_pos_y;
};

class TileMapModel : public AbstractMapModel {
protected:
    int  rowCount(const QModelIndex& = QModelIndex()) const { return 30; }
    int  columnCount(const QModelIndex& = QModelIndex()) const { return 40; }

    uint8_t Tile(uint16_t n) const;
    void    SetTile(uint16_t n, uint8_t data);
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
