#ifndef PALETTEVIEW_HH_
#define PALETTEVIEW_HH_

#include <QAbstractTableModel>
#include <QFont>
#include <QWidget>

#include "updatable.hh"

class PaletteView : public QWidget, IUpdatable {
    Q_OBJECT

    class Model : public QAbstractTableModel, IUpdatable {
    public:
                      Model();
        void          Update();

        QVariant      data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    protected:
        QVariant      headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

        int           rowCount(const QModelIndex& = QModelIndex()) const { return 16; }
        int           columnCount(const QModelIndex& = QModelIndex()) const { return 16; }
        Qt::ItemFlags flags(const QModelIndex&) const { return Qt::ItemIsSelectable | Qt::ItemIsEnabled; }
    private:
        QFont         _font;
    };

public:
    PaletteView();

    void Update();

private:
    Model*      _model;
    class QTableView* _palette;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
