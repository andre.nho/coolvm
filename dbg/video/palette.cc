#include "palette.hh"

#include <QColor>

#include "coolvm.hh"
#include "global.hh"

Palette::Palette()
    : QVector<QRgb>(256)
{
    Update();
}

void
Palette::Update()
{
    for (int i=0; i<256; ++i) {
        Color c = G::coolvm()->video.PaletteColor(i);
        (*this)[i] = QColor(c.r, c.g, c.b).rgb();
    }
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
