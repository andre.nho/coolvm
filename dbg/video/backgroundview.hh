#ifndef BACKGROUNDVIEW_HH_
#define BACKGROUNDVIEW_HH_

#include <QScrollArea>

#include "updatable.hh"

class BackgroundView : public QScrollArea, IUpdatable {
    Q_OBJECT
public:
    BackgroundView(class Palette const& palette);

    void Update();

private:
    void LayoutWidgets();
    void DrawBackground(uint16_t w, uint16_t h, class QGraphicsScene* scene);

    class Palette const& _palette;
    class HexSpinBox *_border_color,
                     *_background_color;
    class QFrame     *_border_frame,
                     *_background_frame;
    class SpinBox    *_pos_x,
                     *_pos_y;
    class QCheckBox  *_double_size;
    class QGraphicsView *_background;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
