#include "backgroundview.hh"

#include <iostream>
#include <vector>
using namespace std;

#include <QCheckBox>
#include <QFrame>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QSizePolicy>

#include "coolvm.hh"
#include "global.hh"
#include "mainwindow.hh"
#include "palette.hh"
#include "spinbox.hh"

BackgroundView::BackgroundView(Palette const& palette)
    : _palette(palette),
      _border_color(new HexSpinBox("0x%02X")), _background_color(new HexSpinBox("0x%02X")), 
      _border_frame(new QFrame), _background_frame(new QFrame),
      _pos_x(new SpinBox), _pos_y(new SpinBox),
      _double_size(new QCheckBox("Double size")),
      _background(new QGraphicsView)
{
    // widget configurations
    _border_color->setRange(0x00, 0xFF);
    _border_color->setWrapping(true);
    _background_color->setRange(0x00, 0xFF);
    _background_color->setWrapping(true);
    for(auto& frame: vector<QFrame*> { _border_frame, _background_frame }) {
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Plain);
        frame->setMinimumSize(30, 20);
        frame->setAutoFillBackground(true);
    }
    _pos_x->setRange(0, G::coolvm()->video.WIDTH * 2 - 1);
    _pos_x->setWrapping(true);
    _pos_y->setRange(0, G::coolvm()->video.HEIGHT * 2 - 1);
    _pos_y->setWrapping(true);
    _background->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    _background->setMinimumSize(Video::WIDTH * 2 + 2, Video::HEIGHT * 2 + 2);

    // events
    connect(_border_color, qOverload<int>(&QSpinBox::valueChanged), [&](int n) {
        G::coolvm()->video.SetBorderColor(static_cast<uint8_t>(n));
        G::mw()->Update();
    });
    connect(_background_color, qOverload<int>(&QSpinBox::valueChanged), [&](int n) {
        G::coolvm()->video.SetBackgroundColor(static_cast<uint8_t>(n));
        G::mw()->Update();
    });
    connect(_pos_x, qOverload<int>(&QSpinBox::valueChanged), [&](int n) {
        uint16_t x, y;
        G::coolvm()->video.BackgroundDisplacement(x, y);
        G::coolvm()->video.SetBackgroundDisplacement(n, y);
        G::mw()->Update();
    });
    connect(_pos_y, qOverload<int>(&QSpinBox::valueChanged), [&](int n) {
        uint16_t x, y;
        G::coolvm()->video.BackgroundDisplacement(x, y);
        G::coolvm()->video.SetBackgroundDisplacement(x, n);
        G::mw()->Update();
    });
    connect(_double_size, &QCheckBox::stateChanged, [&](int n) {
        G::coolvm()->video.SetDoubleSize(n != Qt::Unchecked);
        G::mw()->Update();
    });

    LayoutWidgets();

    Update();
}


void
BackgroundView::LayoutWidgets()
{
    auto wmain = new QWidget;

    QVBoxLayout *v = new QVBoxLayout;
    
    // config
    auto gr1 = new QGroupBox(tr("Configuration"));
    auto g = new QGridLayout;
    g->addWidget(new QLabel(tr("Border color")), 0, 0);
    g->addWidget(_border_color, 0, 1);
    g->addWidget(_border_frame, 0, 2);
    g->addWidget(new QLabel(tr("Background color")), 0, 3);
    g->addWidget(_background_color, 0, 4);
    g->addWidget(_background_frame, 0, 5);
    g->addWidget(new QLabel(tr("Position x")), 1, 0);
    g->addWidget(_pos_x, 1, 1);
    g->addWidget(new QLabel(tr("Position y")), 1, 3);
    g->addWidget(_pos_y, 1, 4);
    g->addWidget(_double_size, 2, 0, 1, 4);
    gr1->setLayout(g);
    gr1->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    v->addWidget(gr1);

    // background
    auto gr2 = new QGroupBox(tr("Background"));
    gr2->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    auto v2 = new QVBoxLayout;
    v2->addWidget(_background);
    gr2->setLayout(v2);
    v->addWidget(gr2);

    v->addStretch();
    wmain->setLayout(v);
    setWidget(wmain);
}


void
BackgroundView::Update()
{
    uint16_t x, y;
    G::coolvm()->video.BackgroundDisplacement(x, y);
    _pos_x->setValue(x);
    _pos_y->setValue(y);

    // border color
    uint8_t b = G::coolvm()->video.BorderColor();
    _border_color->setValue(b);
    Color border = G::coolvm()->video.PaletteColor(b);
    QPalette pal = _border_frame->palette();
    pal.setColor(QPalette::Background, QColor(border.r, border.g, border.b));
    _border_frame->setPalette(pal);

    // background color
    b = G::coolvm()->video.BackgroundColor();
    _background_color->setValue(b);
    Color bg = G::coolvm()->video.PaletteColor(b);
    pal = _background_frame->palette();
    pal.setColor(QPalette::Background, QColor(bg.r, bg.g, bg.b));
    _background_frame->setPalette(pal);

    // double size
    bool dbl = G::coolvm()->video.DoubleSize();
    _double_size->setChecked(dbl);
    if (dbl) {
        _background->setMinimumSize(Video::WIDTH * 2 + 2, Video::HEIGHT * 2 + 2);
    } else {
        _background->setMinimumSize(Video::WIDTH + 2, Video::HEIGHT + 2);
    }

    // background
    uint16_t w = 320, h = 240;
    if (G::coolvm()->video.DoubleSize()) {
        w *= 2;
        h *= 2;
    }

    QGraphicsScene* scene  = new QGraphicsScene(this);
    _background->setScene(scene);
    _background->setSceneRect(QRect(0, 0, w, h));
    if (G::coolvm()->video.BackgroundColor() != 0xFF) {
        scene->addRect(QRectF(0, 0, w, h), QPen(), QBrush(QColor(bg.r, bg.g, bg.b), Qt::SolidPattern));
    } else {
        DrawBackground(w, h, scene);
    }
    _background->show();
}


void
BackgroundView::DrawBackground(uint16_t w, uint16_t h, QGraphicsScene* scene)
{
    // background image
    QImage image(G::coolvm()->video.BackgroundImage(), w, h, QImage::Format_Indexed8);
    QVector<QRgb> colorMap(256);
    for (int i=0; i<256; ++i) {
        Color c = G::coolvm()->video.PaletteColor(i);
        colorMap[i] = QColor(c.r, c.g, c.b).rgb();
    }
    image.setColorTable(colorMap);
    scene->addPixmap(QPixmap::fromImage(image));

    // draw position square
    uint16_t xn, yn;
    G::coolvm()->video.BackgroundDisplacement(xn, yn);
    int x = xn, y = yn;
    QPen pen(Qt::DotLine);
    pen.setColor(QColor(128, 128, 128));
    scene->addRect(QRectF(x, y, G::coolvm()->video.WIDTH, G::coolvm()->video.HEIGHT), pen);

    // draw complementar square
    if (x + G::coolvm()->video.WIDTH > w) {
        x -= w;
    }
    if (y + G::coolvm()->video.HEIGHT > h) {
        y -= h;
    }
    scene->addRect(QRectF(x, y, G::coolvm()->video.WIDTH, G::coolvm()->video.HEIGHT), pen);
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
