#ifndef PALETTE_HH_
#define PALETTE_HH_

#include <QVector>
#include <QRgb>

#include "updatable.hh"

class Palette : public QVector<QRgb>, IUpdatable {
public:
    Palette();

    void Update();
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
