#include "videotab.hh"

#include <QLabel>
#include <QVBoxLayout>
#include <QTabWidget>

#include "coolvm.hh"
#include "global.hh"
#include "paletteview.hh"
#include "backgroundview.hh"
#include "tilemapping.hh"
#include "tiletable.hh"

VideoTab::VideoTab()
    : _palette_view(new PaletteView), _background_view(new BackgroundView(_palette)),
      _tileset(new TileTable(new TileSetModel)), _tilemap(new TileMap)
{
    auto tabs = new QTabWidget();
    tabs->addTab(_palette_view, tr("&Palette"));
    tabs->addTab(_background_view, tr("&Background"));
    tabs->addTab(_tileset, tr("&Tileset"));
    tabs->addTab(_tilemap, tr("&Tilemap"));
    tabs->addTab(new QLabel("Not implemented"), tr("&Spriteset"));
    tabs->addTab(new QLabel("Not implemented"), tr("&Spritemap"));
    tabs->addTab(new QLabel("Not implemented"), tr("&UIset"));
    tabs->addTab(new QLabel("Not implemented"), tr("&UImap"));
    
    auto layout = new QVBoxLayout();
    layout->addWidget(tabs);
    setLayout(layout);
}


void 
VideoTab::Update()
{
    _palette.Update();
    _palette_view->Update();
    _background_view->Update();
    _tileset->Update();
    _tilemap->Update();
}


// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
