#include "paletteview.hh"

#include <QColorDialog>
#include <QHeaderView>
#include <QTableView>
#include <QVBoxLayout>

#include "coolvm.hh"
#include "global.hh"
#include "mainwindow.hh"

// {{{ MODEL

PaletteView::Model::Model()
    : _font("Monospace", 6)
{
}


QVariant
PaletteView::Model::data(const QModelIndex &index, int role) const
{
    Color c = G::coolvm()->video.PaletteColor((index.row() << 4) | index.column());
    if (role == Qt::BackgroundRole) {
        return QBrush(QColor(c.r, c.g, c.b));
    } else if (role == Qt::ForegroundRole) {
        if ((c.r + c.g + c.g) < 384) {
            return QBrush(Qt::white);
        } else {
            return QBrush(Qt::black);
        }
    } else if (role == Qt::DisplayRole) {
        char buf[7];
        sprintf(buf, "%02X%02X%02X", c.r, c.g, c.b);
        return buf;
    } else if (role == Qt::FontRole) {
        return _font;
    }
    return QVariant();
}


QVariant
PaletteView::Model::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Vertical) {
            char buf[3]; sprintf(buf, "%X_", section);
            return buf;
        } else {
            char buf[3]; sprintf(buf, "_%X", section);
            return buf;
        }
    }
    return QVariant();
}


void
PaletteView::Model::Update()
{
    emit dataChanged(index(0, 0), index(15, 15));
}

// }}}

PaletteView::PaletteView()
    : _model(new Model), _palette(new QTableView)
{
    // model
    _palette->setModel(_model);
    _palette->verticalHeader()->setDefaultSectionSize(38);
    for (int i=0; i<16; ++i) {
        _palette->setColumnWidth(i, 38);
    }

    // double click on table brings color dialog
    connect(_palette, &QAbstractItemView::doubleClicked, [&](QModelIndex const& index) {
        QColor current(_model->data(index, Qt::BackgroundRole).value<QColor>());  // gets picked color

        QColorDialog cd(this);
        QColor new_color = cd.getColor(current, this, "Pick a new color");
        if (new_color.isValid()) {
            G::coolvm()->video.SetPaletteColor((index.row() << 4) | index.column(), { 
                static_cast<uint8_t>(new_color.red()), 
                static_cast<uint8_t>(new_color.green()), 
                static_cast<uint8_t>(new_color.blue()),
            });
            G::mw()->Update();
        }
    });
    
    // layout
    auto layout = new QVBoxLayout;
    layout->addWidget(_palette);
    setLayout(layout);
}

void 
PaletteView::Update()
{
    _model->Update();
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
