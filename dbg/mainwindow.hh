#ifndef MAINWINDOW_HH_
#define MAINWINDOW_HH_

#include <QMainWindow>

#include "updatable.hh"

class MainWindow : public QMainWindow, IUpdatable {
    Q_OBJECT
public:
    MainWindow();

    void Update();

protected:
    void closeEvent(QCloseEvent* event);

private:
    void CreateDock(QWidget* widget, QString const& name, Qt::DockWidgetArea area);
    void CreateActions();

    // dock
    class CodeView*          _codeview;
    class CpuRegisters*      _cpuregisters;
    class ComputerRegisters* _computer_registers;

    // tab
    class MemoryTab*         _memorytab;
    class VideoTab*          _videotab;

    // actions
    class QToolBar*          _toolbar;

    class QMenu* _dock_menu;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
