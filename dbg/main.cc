// #include "mainwindow.hh"

#include <QApplication>

#include "coolvm.hh"
#include "global.hh"
#include "mainwindow.hh"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    Q_INIT_RESOURCE(cooldbg);
    G::mw()->show();
    return app.exec();
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
