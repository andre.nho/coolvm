#include "cpuregisters.hh"

#include <cassert>
#include <iostream>

#include <QCheckBox>
#include <QGridLayout>

#include "coolvm.hh"
#include "global.hh"
#include "mainwindow.hh"

// {{{ CPU REGISTERS MODEL

uint32_t
CpuRegisters::Model::Data(int row) const
{
    return G::coolvm()->cpu.Register[row];
}


void
CpuRegisters::Model::SetData(int row, uint32_t value)
{
    G::coolvm()->cpu.Register[row] = value;
    G::mw()->Update();
}


string
CpuRegisters::Model::HeaderData(int row) const
{
    return _names[row];
}

// }}}

CpuRegisters::CpuRegisters()
    : _registers(new SingleColumnTable(new Model()))
{
    // layout
    auto grid = new QGridLayout();
    grid->addWidget(_registers, 0, 0, 1, 3);

    // flags
    _flags.push_back(new QCheckBox("Y"));
    _flags.push_back(new QCheckBox("V"));
    _flags.push_back(new QCheckBox("Z"));
    _flags.push_back(new QCheckBox("S"));
    _flags.push_back(new QCheckBox("GT"));
    _flags.push_back(new QCheckBox("LT"));

    int i = 0, x = 1, y = 0;
    for(auto& flag: _flags) {
        flag->setProperty("number", i++);
        grid->addWidget(flag, x++, y);
        if (x == 4) {
            x = 1; ++y;
        }
        connect(flag, &QCheckBox::stateChanged, [flag, this](bool value) {
            uint8_t i = flag->property("number").toInt();
            G::coolvm()->cpu.FL &= ~(1 << i);
            if (value) {
                G::coolvm()->cpu.FL |= (1 << i);
            }
            G::mw()->Update();
        });
    }

    auto stretch = new QWidget();
    stretch->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    grid->addWidget(stretch, 3, 1);

    setLayout(grid);
}


void
CpuRegisters::Update()
{
    _registers->Update();

    for(int i=0; i<6; ++i) {
        _flags[i]->setChecked((G::coolvm()->cpu.FL >> i) & 1);
    }
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
