#ifndef COMPUTERREGISTERS_HH_
#define COMPUTERREGISTERS_HH_

#include <cstdint>
#include <string>
#include <vector>
#include <utility>
using namespace std;

#include <QHBoxLayout>

#include "coolvm.hh"
#include "singlecolumntable.hh"

class ComputerRegistersTable : public SingleColumnTable {
    Q_OBJECT

    class ComputerRegistersModel : public MemoryMappedSingleColumnModel {
    private:
        const vector<pair<string, uint32_t>> _registers = {  // {{{
            { "CPUCFG", CPUCFG },
            { "CPUINT", CPUINT },
            { "VIDCFG", VIDCFG },
            { "VIDPAL", VIDPAL },
            { "VIDBKG", VIDBKG },
            { "VIDBKRL", VIDBKRL },
            { "VIDTSET", VIDTSET },
            { "VIDTMAP", VIDTMAP },
            { "VIDTMRL", VIDTMRL },
            { "VIDSTSZ", VIDSTSZ },
            { "VIDUSET", VIDUSET },
            { "VIDUMAP", VIDUMAP },
            { "VIDSPSET", VIDSPSET },
            { "VIDSPMAP", VIDSPMAP },
            { "VIDSPCNT", VIDSPCNT },
            { "VIDSCRL", VIDSCRL },
            { "VIDCOL", VIDCOL },
            { "SNDCF0", SNDCF0 },
            { "SNDCF1", SNDCF1 },
            { "SNDCF2", SNDCF2 },
            { "SNDCF3", SNDCF3 },
            { "SNDCF4", SNDCF4 },
            { "SNDCF5", SNDCF5 },
            { "SNDCF6", SNDCF6 },
            { "SNDCF7", SNDCF7 },
            { "SNDCUS0", SNDCUS0 },
            { "SNDCUS1", SNDCUS1 },
            { "SNDCUS2", SNDCUS2 },
            { "SNDCUS3", SNDCUS3 },
            { "SNDCUS4", SNDCUS4 },
            { "SNDCUS5", SNDCUS5 },
            { "SNDCUS6", SNDCUS6 },
            { "SNDCUS7", SNDCUS7 },
            { "SNDENV0", SNDENV0 },
            { "SNDENV1", SNDENV1 },
            { "SNDENV2", SNDENV2 },
            { "SNDENV3", SNDENV3 },
            { "SNDENV4", SNDENV4 },
            { "SNDENV5", SNDENV5 },
            { "SNDENV6", SNDENV6 },
            { "SNDENV7", SNDENV7 },
            { "TIMCFG", TIMCFG },
            { "TIMFRM", TIMFRM },
            { "TIMWALL", TIMWALL },
            { "TIMA", TIMA },
            { "TIMB", TIMB },
            { "KEYEVT", KEYEVT },
            { "JOY0", JOY0 },
            { "JOY1", JOY1 },
            { "MOUPOS", MOUPOS },
            { "MOUBTN", MOUBTN },
            { "DRVDPOS", DRVDPOS },
            { "DRVMPOS", DRVMPOS },
            { "DRVSZ", DRVSZ },
            { "DRVCMD", DRVCMD },
        };  // }}}
    protected:
        string   HeaderData(int row) const { return _registers[row].first; }
        uint32_t Address(int row) const { return _registers[row].second; }
        int      rowCount(const QModelIndex& = QModelIndex()) const { return _registers.size(); }
    };

public:
    ComputerRegistersTable() : SingleColumnTable(new ComputerRegistersModel()) {
        setFixedWidth(180);
    }
};

class ComputerRegisters : public QWidget {
    Q_OBJECT
public:
    ComputerRegisters() : _tbl(new ComputerRegistersTable()) {
        auto box = new QHBoxLayout();
        box->addWidget(_tbl);
        setLayout(box);
    }

    void Update() {
        _tbl->Update();
    }

private:
    ComputerRegistersTable* _tbl;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
