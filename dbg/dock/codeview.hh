#ifndef CODEVIEW_HH_
#define CODEVIEW_HH_

#include <string>
#include <vector>
using namespace std;

#include <QWidget>
#include <QAbstractTableModel>

#include "updatable.hh"

class CodeView : public QWidget, IUpdatable {
    Q_OBJECT

    class Model : public QAbstractTableModel, IUpdatable {  // {{{
        static const int NUMBER_OF_ROWS = 512;
    public:
        Model(class QTableView* code, class HexSpinBox const* address);
        QVariant data(const QModelIndex&, int) const;
        QVariant headerData(int section, Qt::Orientation orientation, int role) const;
        void     Update();
            
        Qt::ItemFlags flags(const QModelIndex&) const;
        int rowCount(const QModelIndex& =QModelIndex()) const { return NUMBER_OF_ROWS; }
        int columnCount(const QModelIndex& =QModelIndex()) const { return 2; }

    private:
        class QTableView* _code;
        class HexSpinBox const* _address;

        struct Instruction {
            uint32_t address;
            string   instruction;
            uint8_t  sz;
        };
        vector<Instruction> _data;
    };   // }}}

public:
    CodeView();

    QSize sizeHint() const { return QSize(280, 0); }

public slots:
    void Update();

private:
    HexSpinBox* _address;
    QTableView* _code;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
