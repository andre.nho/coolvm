#ifndef CPUREGISTERS_HH_
#define CPUREGISTERS_HH_

#include <string>
#include <vector>
using namespace std;

#include <QTableView>
#include <QWidget>

#include "singlecolumntable.hh"
#include "updatable.hh"

class CpuRegisters : public QWidget, IUpdatable {
    Q_OBJECT

    class Model : public SingleColumnModel {  // {{{
    public:
        int rowCount(const QModelIndex& = QModelIndex()) const { return 16; }
    protected:
        uint32_t Data(int row) const;
        void     SetData(int row, uint32_t value);
        string   HeaderData(int row) const;
    private:
        const vector<string> _names {
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "FP", "SP", "PC", "FL" 
        };
    };  // }}}

public:
    CpuRegisters();

    void Update();

private:
    class SingleColumnTable* _registers;
    vector<class QCheckBox*> _flags;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
