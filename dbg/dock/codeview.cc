#include "codeview.hh"

#include <QTableView>
#include <QHBoxLayout>
#include <QLabel>
#include <QHeaderView>
#include <QSpinBox>
#include <QVBoxLayout>

#include "coolvm.hh"
#include "global.hh"
#include "spinbox.hh"

// {{{ MODEL

CodeView::Model::Model(QTableView* code, HexSpinBox const* address)
    : _code(code), _address(address)
{
    Update();
}


QVariant
CodeView::Model::data(const QModelIndex& index, int role) const
{
    if (role == Qt::DisplayRole) {
        if (index.column() == 1) {
            return _data[index.row()].instruction.c_str();
        }
    } else if (role == Qt::CheckStateRole) {
        if (index.column() == 0) {
            return Qt::Unchecked;
        }
    } else if (role == Qt::FontRole) {
        return *G::monospace();
    } else if (role == Qt::TextAlignmentRole) {
        return Qt::AlignLeft + Qt::AlignVCenter;
    } else if (role == Qt::BackgroundRole) {
        if (_data[index.row()].address == G::coolvm()->cpu.PC) {
            return QBrush(QColor(200, 255, 200));
        }
    }
    return QVariant();
}

QVariant
CodeView::Model::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            if (section == 0) {
                return "Bk";
            } else if (section == 1) {
                return "Instruction";
            }
        } else if (orientation == Qt::Vertical) {
            if (!_data.empty()) {
                char buf[20]; long_hex(buf, _data[section].address);
                return buf;
            }
        }
    } else if (role == Qt::FontRole) {
        return *G::monospace();
    }
    return QVariant();
}

Qt::ItemFlags
CodeView::Model::flags(const QModelIndex& index) const 
{ 
    if (index.column() == 0) {
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
    } 
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

void
CodeView::Model::Update()
{
    // reload instructions
    int pc = 0;
    uint32_t addr = _address->value();

    _data.clear();
    for (uint32_t i = 0; i < NUMBER_OF_ROWS; ++i) {
        if (addr == G::coolvm()->cpu.PC) {
            pc = i;
        }
        if (addr < G::coolvm()->MemorySize) {
            auto d = G::coolvm()->cpu.DebugInstruction(addr);
            _data.push_back(Instruction { addr, d.description, d.sz });
            addr += d.sz;
        } else {
            _data.push_back(Instruction { addr, "Out of memory", 1});
            ++addr;
        }
    }

    emit headerDataChanged(Qt::Vertical, 0, NUMBER_OF_ROWS);
    emit dataChanged(index(0, 0), index(rowCount(), columnCount()));
    _code->scrollTo(index(pc, 0));
}

// }}}

CodeView::CodeView()
    : _address(new HexSpinBox("0x%08X")), _code(new QTableView())
{
    // table
    _code->setModel(new Model(_code, _address));
    _code->verticalHeader()->setDefaultSectionSize(20);
    _code->setColumnWidth(0, 22);
    //_code->setColumnWidth(1, 160);
    _code->horizontalHeader()->setStretchLastSection(true);
    _code->setSelectionMode(QAbstractItemView::SingleSelection);

    // address
    _address->setRange(0, G::coolvm()->MemorySize);
    connect(_address, SIGNAL(valueChanged(int)), this, SLOT(Update()));

    // label
    auto lbl = new QLabel(tr("Starting address"));
    lbl->setBuddy(_address);

    // starting address
    auto w1 = new QWidget();
    auto hb = new QHBoxLayout;
    hb->addStretch();
    hb->addWidget(lbl);
    hb->addWidget(_address);
    w1->setLayout(hb);

    // table
    auto vb = new QVBoxLayout;
    vb->addWidget(_code);
    vb->addWidget(w1);
    setLayout(vb);
}


void 
CodeView::Update()
{
    dynamic_cast<Model*>(_code->model())->Update();
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
