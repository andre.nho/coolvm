#ifndef UPDATABLE_HH_
#define UPDATABLE_HH_

class IUpdatable {
public:
    virtual void Update() = 0;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
