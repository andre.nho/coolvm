#include "resourcemanager.hh"

// TODO - add cache

#include <cassert>

#include <QImage>
#include <QVector>
#include <QRgb>

#include "global.hh"
#include "coolvm.hh"

QPixmap 
ResourceManager::TilePixmap(uint16_t n)
{
    QImage image(G::coolvm()->video.TileImage(n), 16, 16, QImage::Format_Indexed8);
    QVector<QRgb> colorMap(256);
    for (int i=0; i<256; ++i) {
        Color c = G::coolvm()->video.PaletteColor(i);
        colorMap[i] = QColor(c.r, c.g, c.b).rgb();
    }
    image.setColorTable(colorMap);
    assert(!image.isNull());
    auto pix = QPixmap::fromImage(image);
    assert(!pix.isNull());
    return pix;
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
