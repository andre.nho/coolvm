#ifndef SINGLECOLUMNTABLE_HH_
#define SINGLECOLUMNTABLE_HH_

#include <string>
#include <vector>
using namespace std;

#include <QTableView>
#include <QAbstractTableModel>

#include "updatable.hh"

class SingleColumnModel : public QAbstractTableModel, IUpdatable {
    Q_OBJECT
public:
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    virtual bool     setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    virtual int columnCount(const QModelIndex& = QModelIndex()) const { return 1; }
    virtual Qt::ItemFlags flags(const QModelIndex&) const { return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled; }

    virtual void     Update();

    void     setDigits(uint8_t digits);

protected:
    virtual uint32_t Data(int row) const = 0;
    virtual void     SetData(int row, uint32_t value) = 0;
    virtual string   HeaderData(int row) const = 0;

    uint8_t  _digits = 8;
};


class MemoryMappedSingleColumnModel : public SingleColumnModel {
    Q_OBJECT
protected:
    uint32_t Data(int row) const;
    void     SetData(int row, uint32_t value);

    virtual uint32_t Address(int row) const = 0;
};


class SingleColumnTable : public QTableView, IUpdatable {
    Q_OBJECT
public:
    SingleColumnTable(SingleColumnModel* model);

    void Update() { _model->Update(); }

private:
    SingleColumnModel* _model;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
