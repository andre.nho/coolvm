#include "singlecolumntable.hh"

#include <cassert>

#include <QHeaderView>

#include "coolvm.hh"
#include "global.hh"
#include "mainwindow.hh"

// {{{ SINGLE COLUMN MODEL

QVariant
SingleColumnModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Vertical) {
            return HeaderData(section).c_str();
        }
    } else if (role == Qt::FontRole) {
        return *G::monospace();
    }
    return QVariant();
}

QVariant 
SingleColumnModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole) {
        if (index.column() == 0) {
            char buf[20] = "error";
            uint32_t value = Data(index.row());
            if (_digits == 2) {
                sprintf(buf, "%02X", static_cast<uint8_t>(value));
            } else if (_digits == 4) {
                sprintf(buf, "%04X", static_cast<uint16_t>(value));
            } else if (_digits == 8) {
                long_hex(buf, value);
            }
            return buf;
        }
    } else if (role == Qt::TextAlignmentRole) {
        return Qt::AlignHCenter + Qt::AlignVCenter;
    } else if (role == Qt::FontRole) {
        return *G::monospace();
    }
    return QVariant();
}


bool
SingleColumnModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::EditRole) {
        if (index.column() == 0) {
            bool ok;
            qint64 ivalue = value.toString().toLongLong(&ok, 16);
            if (ok) {
                SetData(index.row(), static_cast<uint32_t>(ivalue));
                G::mw()->Update();
            } 
            return ok;
        }
    }
    return false;
}


void
SingleColumnModel::setDigits(uint8_t digits)
{
    assert(digits == 2 || digits == 4 || digits == 8);
    _digits = digits;
    Update();
}


void
SingleColumnModel::Update()
{
    emit headerDataChanged(Qt::Vertical, 0, rowCount());
    emit dataChanged(index(0, 0), index(rowCount(), 1));
}

// }}}

// {{{ MEMORY MAPPED SINGLE COLUMN MODEL

uint32_t
MemoryMappedSingleColumnModel::Data(int row) const
{
    if (_digits == 2) {
        return G::coolvm()->Get(Address(row));
    } else if (_digits == 4) {
        return G::coolvm()->Get16(Address(row));
    } else if (_digits == 8) {
        return G::coolvm()->Get32(Address(row));
    }
    assert(false);
}


void
MemoryMappedSingleColumnModel::SetData(int row, uint32_t value)
{
    if (_digits == 2) {
        G::coolvm()->Set(Address(row), static_cast<uint8_t>(value));
    } else if (_digits == 4) {
        G::coolvm()->Set16(Address(row), static_cast<uint16_t>(value));
    } else if (_digits == 8) {
        G::coolvm()->Set32(Address(row), value);
    } else {
        assert(false);
    }
}

// }}}

// {{{ SINGLE COLUMN TABLE

SingleColumnTable::SingleColumnTable(SingleColumnModel* model)
    : _model(model)
{
    setModel(model);
    setFixedWidth(140);
    verticalHeader()->setDefaultSectionSize(20);
    setColumnWidth(0, 74);
    horizontalHeader()->hide();
    horizontalHeader()->setStretchLastSection(true);
}

// }}}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
