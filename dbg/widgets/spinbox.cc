#include "spinbox.hh"

#include <cassert>
#include <cstdio>
#include <iostream>
using namespace std;

#include <QFont>

#include "global.hh"

SpinBox::SpinBox()
{
    setFont(*G::monospace());
    setAlignment(Qt::AlignRight);
}


HexSpinBox::HexSpinBox(const char* fmt)
    : _fmt(fmt)
{
    setDisplayIntegerBase(16);
}


QString
HexSpinBox::textFromValue(int value) const
{
    char buf[40];
    snprintf(buf, sizeof buf, _fmt, value);
    return buf;
}


// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
