#ifndef HEXSPINBOX_HH_
#define HEXSPINBOX_HH_

#include <QObject>
#include <QSpinBox>

#include <cstdint>

class SpinBox : public QSpinBox {
    Q_OBJECT
public:
    SpinBox();
};

class HexSpinBox : public SpinBox {
    Q_OBJECT
public:
    HexSpinBox(const char* fmt);

protected:
    QString textFromValue(int value) const;

private:
    const char* _fmt;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
