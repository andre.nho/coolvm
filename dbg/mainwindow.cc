#include "mainwindow.hh"

#include <iostream>

#include <QDockWidget>
#include <QLabel>
#include <QMenuBar>
#include <QToolBar>
#include <QHBoxLayout>

#include "codeview.hh"
#include "memorytab.hh"
#include "cpuregisters.hh"
#include "computerregisters.hh"
#include "videotab.hh"

#include "coolvm.hh"
#include "global.hh"

MainWindow::MainWindow()
    : _codeview(new CodeView()),
      _cpuregisters(new CpuRegisters()),
      _computer_registers(new ComputerRegisters()),
      _memorytab(new MemoryTab()),
      _videotab(new VideoTab())
{
    // title
    setWindowTitle("CoolVM Debugger (" DBG_VERSION ")");

    // actions
    CreateActions();

    // dock widgets
    CreateDock(_codeview, tr("Code Execution"), Qt::LeftDockWidgetArea);
    CreateDock(_cpuregisters, tr("CPU Registers"), Qt::RightDockWidgetArea);
    CreateDock(_computer_registers, tr("Computer Registers"), Qt::RightDockWidgetArea);

    // central widget
    auto tabs = new QTabWidget();
    tabs->addTab(_memorytab, tr("&Memory"));
    tabs->addTab(_videotab, tr("V&ideo"));
    tabs->addTab(new QLabel("Not implemented"), tr("&Sound"));
    tabs->addTab(new QLabel("Not implemented"), tr("&Timers"));
    tabs->addTab(new QLabel("Not implemented"), tr("&Inputs"));
    tabs->addTab(new QLabel("Not implemented"), tr("&Disk"));
    setCentralWidget(tabs);

    Update();
}


void 
MainWindow::CreateDock(QWidget* widget, QString const& name, Qt::DockWidgetArea area)
{
    // create dock
    auto dock = new QDockWidget(name, this);
    dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    dock->setWidget(widget);
    addDockWidget(area, dock);
    _dock_menu->addAction(dock->toggleViewAction());
}


void
MainWindow::CreateActions()
{
    // other actions
    QAction* step = new QAction(QIcon(":/step.png"), "Step", this);
    connect(step, &QAction::triggered, [&](){
        G::coolvm()->Step();
        Update();
    });

    // menu
    _dock_menu = menuBar()->addMenu(tr("&View"));
    auto debugMenu = menuBar()->addMenu(tr("&Debug"));
    debugMenu->addAction(step);

    // toolbar
    _toolbar = addToolBar("Debug");
    _toolbar->addAction(step);
}


void 
MainWindow::closeEvent(QCloseEvent*)
{
    _toolbar->clear();
}


void
MainWindow::Update()
{
    _cpuregisters->Update();
    _computer_registers->Update();
    _memorytab->Update();
    _videotab->Update();
    _codeview->Update();  // this is slow
}


// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
