#ifndef RESOURCEMANAGER_HH_
#define RESOURCEMANAGER_HH_

#include <QPixmap>

class ResourceManager {
public:
    QPixmap TilePixmap(uint16_t n);
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
