#include "memorytab.hh"

#include <QGridLayout>

#include "interruptview.hh"
#include "memorydataview.hh"
#include "stackview.hh"

MemoryTab::MemoryTab()
    : _memorydata_view(new MemoryDataView), _stack_view(new StackView),
      _interrupt_view(new InterruptView)
{
    auto grid = new QGridLayout;
    grid->setColumnStretch(1, 1);
    grid->addWidget(_memorydata_view, 0, 0, 1, 2);
    grid->addWidget(_stack_view, 0, 2, 2, 1);
    grid->addWidget(_interrupt_view, 1, 0);
    setLayout(grid);
}

void 
MemoryTab::Update()
{
    _memorydata_view->Update();
    _stack_view->Update();
    _interrupt_view->Update();
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
