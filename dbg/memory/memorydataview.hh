#ifndef MEMORYDATAVIEW_HH_
#define MEMORYDATAVIEW_HH_

#include <QGroupBox>
#include <QAbstractTableModel>

#include "updatable.hh"

class MemoryDataView : public QGroupBox, IUpdatable {
    Q_OBJECT

    class Memory : public QAbstractTableModel, IUpdatable {  // {{{
    public:
        Memory(class HexSpinBox const* base) : _base(base) {}
        void Update();
    protected:
        QVariant      headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
        QVariant      data(const QModelIndex &index, int role = Qt::DisplayRole) const;
        bool          setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
        int           rowCount(const QModelIndex& = QModelIndex()) const { return 16; }
        int           columnCount(const QModelIndex& = QModelIndex()) const { return 17; }
        Qt::ItemFlags flags(const QModelIndex& index) const;
    private:
        uint32_t      Address(const QModelIndex& index) const;
        class HexSpinBox const* _base;
    };  // }}}

public:
    MemoryDataView();
    void Update();

private:
    class HexSpinBox* _base;
    class Memory*     _memory_model;
    class QTableView* _memory;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
