#include "memorydataview.hh"

#include <QGridLayout>
#include <QGroupBox>
#include <QHeaderView>
#include <QLabel>
#include <QTableView>

#include "coolvm.hh"
#include "global.hh"
#include "mainwindow.hh"
#include "spinbox.hh"

// {{{ MEMORY VIEW MODEL

QVariant
MemoryDataView::Memory::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            if (section != 16) {
                char buf[3]; sprintf(buf, "_%X", section);
                return buf;
            } else {
                return "Data";
            }
        } else {
            char buf[20]; long_hex(buf, (_base->value() << 8) + (section << 4));
            buf[8] = '_';
            return buf;
        }
    } else if (role == Qt::FontRole) {
        return *G::monospace();
    }
    return QVariant();
}


QVariant
MemoryDataView::Memory::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole) {
        if (index.column() != 16) {
            char buf[3]; sprintf(buf, "%02X", G::coolvm()->Get(Address(index)));
            return buf;
        } else {
            char buf[9] = { 0 };
            for (int i=0; i<8; ++i) {
                uint8_t data = G::coolvm()->Get(Address(index) - 16 + i);
                buf[i] = (data >= 32 && data < 127) ? data : '.';
            }
            return buf;
        }
    } else if (role == Qt::TextAlignmentRole) {
        return Qt::AlignHCenter + Qt::AlignVCenter;
    } else if (role == Qt::FontRole) {
        return *G::monospace();
    }
    return QVariant();
}


bool
MemoryDataView::Memory::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::EditRole) {
        bool ok;
        int ivalue = value.toString().toInt(&ok, 16);
        if (ok) {
            G::coolvm()->Set(Address(index), static_cast<uint8_t>(ivalue));
            G::mw()->Update();
        } 
        return ok;
    }
    return false;
}


Qt::ItemFlags
MemoryDataView::Memory::flags(const QModelIndex& index) const
{
    if (index.column() == 16) {
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    } else {
        return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
    }
}


uint32_t
MemoryDataView::Memory::Address(const QModelIndex& index) const
{
    return (_base->value() << 8) + (index.row() << 4) + index.column();
}


void
MemoryDataView::Memory::Update()
{
    emit headerDataChanged(Qt::Vertical, 0, 16);
    emit dataChanged(index(0, 0), index(16, 17));
}

// }}}

MemoryDataView::MemoryDataView()
    : QGroupBox(tr("Memory")), _base(new HexSpinBox("0x%06X")), _memory_model(new Memory(_base)),
      _memory(new QTableView())
{
    _memory->setModel(_memory_model);
    _memory->verticalHeader()->setDefaultSectionSize(20);
    for (int i=0; i<16; ++i) {
        _memory->setColumnWidth(i, 22);
    }
    _memory->setColumnWidth(16, 70);
    _memory->setFixedHeight(356);

    _base->setRange(0, (G::coolvm()->MemorySize - 1) >> 8);
    connect(_base, qOverload<int>(&QSpinBox::valueChanged), [=]() { _memory_model->Update(); });

    auto grid = new QGridLayout;
    grid->setColumnStretch(0, 1);
    grid->addWidget(new QLabel(tr("Base address")), 0, 1);
    grid->addWidget(_base, 0, 2);
    grid->addWidget(_memory, 1, 0, 1, 3);
    setLayout(grid);
}


void
MemoryDataView::Update()
{
    _memory_model->Update();
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
