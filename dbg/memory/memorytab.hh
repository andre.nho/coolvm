#ifndef MEMORYTAB_HH_
#define MEMORYTAB_HH_

#include <QWidget>

#include "updatable.hh"

class MemoryTab : public QWidget, IUpdatable {
    Q_OBJECT
public:
    MemoryTab();

    void Update();

private:
    class MemoryDataView* _memorydata_view;
    class StackView*      _stack_view;
    class InterruptView*  _interrupt_view;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
