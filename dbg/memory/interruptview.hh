#ifndef INTERRUPTVIEW_HH_
#define INTERRUPTVIEW_HH_

#include <string>
#include <vector>
using namespace std;

#include <QGroupBox>

#include "singlecolumntable.hh"
#include "updatable.hh"

class InterruptView : public QGroupBox, IUpdatable {
    Q_OBJECT

    class Model : public SingleColumnModel {
    protected:
        uint32_t Data(int row) const;
        void     SetData(int row, uint32_t value);
        string   HeaderData(int row) const;
        int      rowCount(const QModelIndex &parent = QModelIndex()) const;
        // extra column
        int      columnCount(const QModelIndex& = QModelIndex()) const { return 2; }
        QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
        bool     setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
        Qt::ItemFlags flags(const QModelIndex &index) const;
    private:
        const vector<string> _names {
            "IINVOP",
            "ISCREEN", "ITIMA", "ITIMB", "IKEY", "IJOY", "IMOUMOV", "IMOUBTN",
            "IDRIVE0", "IDRIVE1", "IDRIVE2", "IDRIVE3",
        };
    };

public:
    InterruptView();

    void Update();

private:
    Model*             _model;
    SingleColumnTable* _interrupts;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
