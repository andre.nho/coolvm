#include "interruptview.hh"

#include <QVBoxLayout>
#include <QHeaderView>

#include "coolvm.hh"
#include "global.hh"
#include "mainwindow.hh"

// {{{ INTERRUPT MODEL

int
InterruptView::Model::rowCount(const QModelIndex&) const
{
    return _names.size();
}


uint32_t
InterruptView::Model::Data(int row) const
{
    return G::coolvm()->cpu.InterruptPointer(static_cast<CPU::Interrupt>(row));
}


void
InterruptView::Model::SetData(int row, uint32_t value)
{
    G::coolvm()->cpu.SetInterruptPointer(static_cast<CPU::Interrupt>(row), value);
}


string
InterruptView::Model::HeaderData(int row) const
{
    return _names[row];
}

// extra columns

QVariant
InterruptView::Model::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        if (section == 0) {
            return "Address";
        } else {
            return "Fired";
        }
    }
    return SingleColumnModel::headerData(section, orientation, role);
}


QVariant 
InterruptView::Model::data(const QModelIndex &index, int role) const
{
    if (index.column() == 1 && role == Qt::CheckStateRole) {
        return G::coolvm()->cpu.InterruptFired(static_cast<CPU::Interrupt>(index.row())) ? Qt::Checked : Qt::Unchecked;
    }
    return SingleColumnModel::data(index, role);
}


#include <iostream>
bool
InterruptView::Model::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.column() == 1 && role == Qt::CheckStateRole) {
        G::coolvm()->cpu.SetInterruptFired(static_cast<CPU::Interrupt>(index.row()), value == Qt::Checked);
        G::mw()->Update();
    }
    return SingleColumnModel::setData(index, value, role);
}


Qt::ItemFlags 
InterruptView::Model::flags(const QModelIndex &index) const
{
    if (index.column() == 1) {
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
    }
    return SingleColumnModel::flags(index);
}

// }}}

InterruptView::InterruptView()
    : QGroupBox(tr("Interrupts")), _model(new Model), _interrupts(new SingleColumnTable(_model))
{
    _interrupts->setFixedWidth(205);
    _interrupts->setColumnWidth(1, 24);
    _interrupts->horizontalHeader()->show();
    
    auto layout = new QVBoxLayout;
    layout->addWidget(_interrupts);
    setLayout(layout);
}


void
InterruptView::Update()
{
    _model->Update();
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
