#include "stackview.hh"

#include <QGridLayout>
#include <QLabel>
#include <QTableView>
#include <QComboBox>

#include "coolvm.hh"
#include "global.hh"

// {{{ STACK MODEL

int
StackView::Model::rowCount(const QModelIndex&) const
{
    return 64;
}

uint32_t
StackView::Model::Address(int row) const
{
    return (G::coolvm()->cpu.SP - 1 - (row * (_digits/2))) % G::coolvm()->MemorySize;
}

string
StackView::Model::HeaderData(int row) const
{
    char buf[20]; long_hex(buf, Address(row));
    return buf;
}

// }}}

#include <iostream>
StackView::StackView()
    : QGroupBox(tr("Stack")), _model(new Model), _stack(new SingleColumnTable(_model)),
      _width(new QComboBox())
{
    _stack->setFixedWidth(185);

    _width->insertItem(0, tr("8 bits"));
    _width->insertItem(1, tr("16 bits"));
    _width->insertItem(2, tr("32 bits"));
    _width->setCurrentIndex(2);

    connect(_width, qOverload<int>(&QComboBox::activated), [&](int i) {
        if (i == 0) {
            _model->setDigits(2);
        } else if (i == 1) {
            _model->setDigits(4);
        } else if (i == 2) {
            _model->setDigits(8);
        }
        _model->Update();
    });

    auto grid = new QGridLayout;
    grid->setColumnStretch(0, 1);
    grid->addWidget(_stack, 0, 0, 1, 3);
    grid->addWidget(new QLabel(tr("Width")), 1, 1);
    grid->addWidget(_width, 1, 2);
    setLayout(grid);
}

void
StackView::Update()
{
    _model->Update();
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
