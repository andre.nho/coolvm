#ifndef STACKVIEW_HH_
#define STACKVIEW_HH_

#include <QGroupBox>

#include "singlecolumntable.hh"
#include "updatable.hh"

class StackView : public QGroupBox, IUpdatable {
    Q_OBJECT

    class Model : public MemoryMappedSingleColumnModel {
    protected:
        uint32_t Address(int row) const;
        string   HeaderData(int row) const;
        int      rowCount(const QModelIndex &parent = QModelIndex()) const;
    };

public:
    StackView();

    void Update();

private:
    class Model*             _model;
    class SingleColumnTable* _stack;
    class QComboBox*         _width;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
