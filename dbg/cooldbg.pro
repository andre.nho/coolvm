# version
DEFINES += DBG_VERSION=\\\"0.0.1\\\"

# source
HEADERS += global.hh updatable.hh mainwindow.hh resourcemanager.hh		\
           dock/codeview.hh dock/computerregisters.hh dock/cpuregisters.hh 	\
	   memory/memorytab.hh memory/memorydataview.hh memory/stackview.hh 	\
	   	memory/interruptview.hh						\
           video/videotab.hh video/paletteview.hh video/backgroundview.hh       \
	   	video/palette.hh video/tiletable.hh video/tilemapping.hh	\
	   widgets/singlecolumntable.hh widgets/spinbox.hh 
SOURCES += global.cc main.cc mainwindow.cc resourcemanager.cc			\
           dock/codeview.cc dock/cpuregisters.cc 				\
	   memory/memorytab.cc memory/memorydataview.cc memory/stackview.cc 	\
	   	memory/interruptview.cc						\
	   video/videotab.cc video/paletteview.cc video/backgroundview.cc       \
	   	video/palette.cc video/tiletable.cc video/tilemapping.cc	\
	   widgets/singlecolumntable.cc widgets/spinbox.cc 

RESOURCES = cooldbg.qrc

#
# add library
#
INCLUDEPATH += ../lib dock memory widgets video
LIBS        += -L../lib -lcoolvm

#
# configuration
#
CONFIG += warn_on c++14 debug
QT     += core widgets gui
TARGET  = cooldbg

#
# custom targets
#
run.depends          = cooldbg
run.commands         = LD_LIBRARY_PATH=../lib ./cooldbg

check-leaks.depends  = cooldbg
check-leaks.commands = LD_LIBRARY_PATH=../lib valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --suppressions=cooldbg.supp ./cooldbg

gen-suppressions.depends  = cooldbg
gen-suppressions.commands = LD_LIBRARY_PATH=../lib valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --error-limit=no --gen-suppressions=all --log-file=cooldbg.supp ./cooldbg && sed -i -e '/^==.*$$/d' cooldbg.supp

QMAKE_EXTRA_TARGETS += run check-leaks gen-suppressions
