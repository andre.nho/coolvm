#include "global.hh"

#include <QGlobalStatic>
#include <QFont>

#include "coolvm.hh"
#include "mainwindow.hh"
#include "resourcemanager.hh"

Q_GLOBAL_STATIC(CoolVM, _coolvm)
Q_GLOBAL_STATIC(MainWindow, _mw)
Q_GLOBAL_STATIC_WITH_ARGS(QFont, _monospace, ("Monospace"));
Q_GLOBAL_STATIC(ResourceManager, _resources)

CoolVM*          G::coolvm() { return _coolvm; }
MainWindow*      G::mw() { return _mw; }
QFont*           G::monospace() { return _monospace; }
ResourceManager* G::resources() { return _resources; }

void long_hex(char buf[20], uint32_t value)
{
    sprintf(buf, "%04X:%04X", value >> 16, value & 0xFFFF);
}


// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
