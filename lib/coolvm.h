#ifndef COOLVM_HH_
#define COOLVM_HH_

typedef struct CoolVM CoolVM;

CoolVM* coolvm_init(uint32_t memory_size);
uint8_t coolvm_get(CoolVM* cvm, uint32_t data);
void    coolvm_set(CoolVM* cvm, uint32_t pos, uint8_t data);
void    coolvm_destroy(CoolVM* cvm);

void     coolvm_cpu_step(CoolVM* cvm);
uint32_t coolvm_cpu_get_register(CoolVM* cvm, uint8_t n);
void     coolvm_cpu_set_register(CoolVM* cvm, uint8_t n, uint32_t value);
uint8_t  coolvm_cpu_debug_instruction(CoolVM* cvm, uint32_t pos, char inst[30]);

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
