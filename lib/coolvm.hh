#ifndef COOLVM_HH_
#define COOLVM_HH_

#include <cstdint>
#include <memory>
using namespace std;

#include "cpu.hh"
#include "video.hh"
#include "registers.hh"

class CoolVM {
public:
    CoolVM(uint32_t memory_size=DEFAULT_MEMORY_SIZE);

private:
    unique_ptr<uint8_t[]> _data;

public:
    uint8_t  Get(uint32_t pos) const;
    void     Set(uint32_t pos, uint8_t data);

    uint16_t Get16(uint32_t pos) const;
    uint32_t Get32(uint32_t pos) const;

    void     Set16(uint32_t pos, uint16_t data);
    void     Set32(uint32_t pos, uint32_t data);

    void     Step();

    uint8_t* DirectMemoryAccess() { return _data.get(); }

    const uint32_t MemorySize;

    CPU cpu;
    Video video;

private:
    const static uint32_t DEFAULT_MEMORY_SIZE = 4 * 1024 * 1024;  // 4 Mb
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
