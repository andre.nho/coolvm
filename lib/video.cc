#include "video.hh"

#include "coolvm.hh"

Video::Video(class CoolVM& coolvm) 
    : _coolvm(coolvm)
{
    _coolvm.Set32(VIDCFG, 0x1);
}

// {{{ PALETTE

Color
Video::PaletteColor(uint8_t n) const
{
    uint32_t color_addr = _coolvm.Get32(VIDPAL) + (n * 3);
    return { 
        _coolvm.Get(color_addr), 
        _coolvm.Get(color_addr+1),
        _coolvm.Get(color_addr+2)
    };
}


void
Video::SetPaletteColor(uint8_t n, Color c)
{
    uint32_t color_addr = _coolvm.Get32(VIDPAL) + (n * 3);
    _coolvm.Set(color_addr, c.r);
    _coolvm.Set(color_addr+1, c.g);
    _coolvm.Set(color_addr+2, c.b);
}

// }}}

// {{{ BACKGROUND

uint8_t
Video::BorderColor() const
{
    return _coolvm.Get(VIDCFG + 2);
}


void
Video::SetBorderColor(uint8_t n) const
{
    _coolvm.Set(VIDCFG + 2, n);
}


uint8_t
Video::BackgroundColor() const
{
    return _coolvm.Get(VIDCFG + 1);
}


void
Video::SetBackgroundColor(uint8_t n) const
{
    _coolvm.Set(VIDCFG + 1, n);
}


void
Video::BackgroundDisplacement(uint16_t& x, uint16_t& y) const
{
    uint32_t v = _coolvm.Get32(VIDBKRL);
    x = v & 0xFFFF;
    y = v >> 16;
}


void    
Video::SetBackgroundDisplacement(uint16_t x, uint16_t y)
{
    _coolvm.Set32(VIDBKRL, (y << 16) | x);
}


bool
Video::DoubleSize() const
{
    return _coolvm.Get(VIDCFG + 3) & 1;
}


void
Video::SetDoubleSize(bool v)
{
    if (v) {
        _coolvm.Set(VIDCFG + 3, _coolvm.Get(VIDCFG + 3) | 1);
    } else {
        _coolvm.Set(VIDCFG + 3, _coolvm.Get(VIDCFG + 3) & ~1);
    }
}


uint8_t const*
Video::BackgroundImage() const
{
    if (BackgroundColor() != 0xFF) {
        return nullptr;
    } else {
        return &_coolvm.DirectMemoryAccess()[_coolvm.Get32(VIDBKG)];
    }
}


// }}}

// {{{ TILES

uint8_t const* 
Video::TileImage(uint16_t n) const
{
    return &_coolvm.DirectMemoryAccess()[_coolvm.Get32(VIDTSET) + (n * 8 * 8)];
}

void
Video::TilesetDisplacement(uint16_t& x, uint16_t& y) const
{
    uint32_t v = _coolvm.Get32(VIDTMRL);
    x = v & 0xFFFF;
    y = v >> 16;
}

void
Video::SetTilesetDisplacement(uint16_t x, uint16_t y)
{
    _coolvm.Set32(VIDTMRL, (y << 16) | x);
}

void
Video::TileSize(int8_t& x, int8_t& y) const
{
    uint32_t v = _coolvm.Get32(VIDSTSZ);
    x = v & 0xFF;
    y = (v >> 8) & 0xFF;
}

void
Video::SetTileSize(int8_t x, int8_t y)
{
    uint32_t v = _coolvm.Get32(VIDSTSZ) & 0xFFFF0000;
    v |= static_cast<uint16_t>(y << 8) | static_cast<uint8_t>(x);
    _coolvm.Set32(VIDSTSZ, v);
}

uint8_t
Video::TileTile(uint16_t n) const
{
    return _coolvm.Get(_coolvm.Get32(VIDTMAP) + n);
}

void
Video::SetTileTile(uint16_t n, uint8_t data)
{
    _coolvm.Set(_coolvm.Get32(VIDTMAP) + n, data);
}

// }}}

// {{{ SPRITES

uint8_t const* 
Video::SpriteImage(uint16_t n) const
{
    return &_coolvm.DirectMemoryAccess()[_coolvm.Get32(VIDSPSET) + (n * 8 * 8)];
}

// }}}

// {{{ UI

uint8_t const* 
Video::UIImage(uint16_t n) const
{
    return &_coolvm.DirectMemoryAccess()[_coolvm.Get32(VIDUSET) + (n * 8 * 8)];
}

// }}}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
