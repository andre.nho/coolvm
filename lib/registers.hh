#ifndef REGISTERS_HH_
#define REGISTERS_HH_

#include <cstdint>

const uint32_t // cpu
               CPUCFG   = 0x7C00,
               CPUINT   = 0x7C04,
               // video
               VIDCFG   = 0x7C08,
               VIDPAL   = 0x7C0C,
               VIDBKG   = 0x7C10,
               VIDBKRL  = 0x7C14,
               VIDTSET  = 0x7C18,
               VIDTMAP  = 0x7C20,
               VIDTMRL  = 0x7C24,
               VIDSTSZ  = 0x7C28,
               VIDUSET  = 0x7C2C,
               VIDUMAP  = 0x7C30,
               VIDSPSET = 0x7C34,
               VIDSPMAP = 0x7C38,
               VIDSPCNT = 0x7C3C,
               VIDSCRL  = 0x7C40,
               VIDCOL   = 0x7C44,
               // sound
               SNDCF0   = 0x7C48,
               SNDCF1   = 0x7C4C,
               SNDCF2   = 0x7C50,
               SNDCF3   = 0x7C54,
               SNDCF4   = 0x7C58,
               SNDCF5   = 0x7C5C,
               SNDCF6   = 0x7C60,
               SNDCF7   = 0x7C64,
               SNDCUS0  = 0x7C68,
               SNDCUS1  = 0x7C6C,
               SNDCUS2  = 0x7C70,
               SNDCUS3  = 0x7C74,
               SNDCUS4  = 0x7C78,
               SNDCUS5  = 0x7C7C,
               SNDCUS6  = 0x7C80,
               SNDCUS7  = 0x7C84,
               SNDENV0  = 0x7C88,
               SNDENV1  = 0x7C8C,
               SNDENV2  = 0x7C90,
               SNDENV3  = 0x7C94,
               SNDENV4  = 0x7C98,
               SNDENV5  = 0x7C9C,
               SNDENV6  = 0x7CA0,
               SNDENV7  = 0x7CA4,
               // timer
               TIMCFG   = 0x7CA8,
               TIMFRM   = 0x7CAC,
               TIMWALL  = 0x7CB0,
               TIMA     = 0x7CB4,
               TIMB     = 0x7CB8,
               // input
               KEYEVT   = 0x7CBC,
               JOY0     = 0x7CC0,
               JOY1     = 0x7CC4,
               MOUPOS   = 0x7CC8,
               MOUBTN   = 0x7CCC,
               // disk access
               DRVDPOS  = 0x7CD0,
               DRVMPOS  = 0x7CD4,
               DRVSZ    = 0x7CD8,
               DRVCMD   = 0x7CDC;

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
