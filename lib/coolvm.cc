#include "coolvm.hh"

#include <cassert>

#include "bios.inc"

CoolVM::CoolVM(uint32_t memory_size)
    : _data(make_unique<uint8_t[]>(memory_size)), MemorySize(memory_size), cpu(*this), video(*this)
{
    assert(bios_bin_len <= memory_size);

    uint32_t pos = 0;
    for (unsigned char byte: bios_bin) {
        _data[pos++] = byte;
    }
}


uint8_t
CoolVM::Get(uint32_t pos) const
{
    return _data[pos];
}


uint16_t
CoolVM::Get16(uint32_t pos) const
{
    return _data[pos] | (_data[pos+1] << 8);
}


uint32_t
CoolVM::Get32(uint32_t pos) const
{
    return _data[pos] | (_data[pos+1] << 8) | (_data[pos+2] << 16) | (_data[pos+3] << 24);
}


void
CoolVM::Set(uint32_t pos, uint8_t data)
{
    _data[pos] = data;
    
    // video.DataChanged(pos);
}


void
CoolVM::Set16(uint32_t pos, uint16_t data)
{
    Set(pos, data & 0xFF);
    Set(pos+1, data >> 8);
}


void
CoolVM::Set32(uint32_t pos, uint32_t data)
{
    Set(pos, data & 0xFF);
    Set(pos+1, (data >> 8) & 0xFF);
    Set(pos+2, (data >> 16) & 0xFF);
    Set(pos+3, (data >> 24) & 0xFF);
}


void
CoolVM::Step()
{
    cpu.Step();
}


// {{{ C INTERFACE

extern "C" {

    CoolVM* coolvm_init(uint32_t memory_size)
    {
        return new CoolVM(memory_size);
    }


    uint8_t coolvm_get(CoolVM* cvm, uint32_t data)
    {
        return cvm->Get(data);
    }

    void coolvm_set(CoolVM* cvm, uint32_t pos, uint8_t data)
    {
        cvm->Set(pos, data);
    }

    void coolvm_step(CoolVM* cvm)
    {
        cvm->Step();
    }

    void coolvm_destroy(CoolVM* cvm)
    {
        delete cvm;
    }

}

// }}}


// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
