#include "coolvm.hh"

#include <cassert>
#include <cstdint>

#include <iomanip>
#include <ios>
#include <iostream>
using namespace std;

int main()
{
    cout << "Running tests..." << endl;

    {
        cout << "Testing memory..." << endl;

        CoolVM coolvm;
        coolvm.Set(0, 0x12);
        assert(coolvm.Get(0) == 0x12);
        coolvm.Set16(0, 0x1234);
        assert(coolvm.Get(0) == 0x34);
        assert(coolvm.Get16(0) == 0x1234);
        coolvm.Set32(0, 0x12345678);
        assert(coolvm.Get32(0) == 0x12345678);
    }

    {
        cout << "Testing CPU debugger..." << endl;

        CoolVM coolvm;
        coolvm.Set(1, 0x12);
        coolvm.Set(2, 0x34);
        coolvm.Set(3, 0x56);
        coolvm.Set(4, 0x78);
        coolvm.Set(5, 0x9A);
        coolvm.Set(6, 0xBC);
        coolvm.Set(7, 0xDE);
        coolvm.Set(8, 0xF0);
        coolvm.Set(9, 0x12);

        for(uint8_t i=0; i<0x80; ++i) {
            coolvm.Set(0, i);
            cout << "0x" << uppercase << setfill('0') << setw(2) << hex << static_cast<int>(i) << ": ";
            cout << coolvm.cpu.DebugInstruction(0).description << endl;
        }
    }

    {
        cout << "Execution..." << endl;
        CoolVM coolvm;
        coolvm.Set(0x0, 0x2);
        coolvm.Set(0x2, 0x5);
        coolvm.Step();
    }

    cout << "Tests finished." << endl;
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
