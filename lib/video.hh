#ifndef VIDEO_HH_
#define VIDEO_HH_

#include <cstdint>
#include <set>
using namespace std;

struct Color {
    uint8_t r, g, b;
};

class Video {
public:
    static const uint16_t WIDTH = 320,
                          HEIGHT = 240;

    Video(class CoolVM& coolvm);

    // palette
    Color PaletteColor(uint8_t n) const;
    void  SetPaletteColor(uint8_t n, Color c);

    // background
    uint8_t BorderColor() const;
    void    SetBorderColor(uint8_t n) const;

    uint8_t BackgroundColor() const;
    void    SetBackgroundColor(uint8_t n) const;

    void    BackgroundDisplacement(uint16_t& x, uint16_t& y) const;
    void    SetBackgroundDisplacement(uint16_t x, uint16_t y);

    bool    DoubleSize() const;
    void    SetDoubleSize(bool v);

    uint8_t const* BackgroundImage() const;

    // tiles
    uint8_t const* TileImage(uint16_t n) const;

    void    TilesetDisplacement(uint16_t& x, uint16_t& y) const;
    void    SetTilesetDisplacement(uint16_t x, uint16_t y);

    void    TileSize(int8_t& x, int8_t& y) const;
    void    SetTileSize(int8_t x, int8_t y);

    uint8_t TileTile(uint16_t n) const;
    void    SetTileTile(uint16_t n, uint8_t data);

    // sprites
    uint8_t const* SpriteImage(uint16_t n) const;

    // UI elements
    uint8_t const* UIImage(uint16_t n) const;


private:
    class CoolVM& _coolvm;
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
