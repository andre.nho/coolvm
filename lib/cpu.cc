#include "cpu.hh"

#include <cassert>
#include <cstdint>

#include <algorithm>
#include <iomanip>
#include <ios>
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

#include "coolvm.hh"
#include "registers.hh"

constexpr const char*     CPU::instruction_name[];
constexpr InstructionInfo CPU::instructions[];
constexpr const char*     CPU::register_name[];

CPU::CPU(class CoolVM& coolvm) 
    : _coolvm(coolvm)
{
    _coolvm.Set32(CPUCFG, VERSION);
}

// {{{ EXECUTE INSTRUCTION

void
CPU::Step()
{
    uint32_t pos = PC;
    InstructionInfo const& inst = instructions[_coolvm.Get(pos++)];

    array<CPU::Parameter, 2> pars = ParseParameters(inst, pos);
    uint8_t sz = InstructionSize(inst);

    switch (inst.instruction) {
        case MOV:  
            Apply(pars[0], Take(pars[1])); 
            break;
        case MOVB: 
            Apply(pars[0], static_cast<uint8_t>(Take(pars[1])), 8); 
            break;
        case MOVW: 
            Apply(pars[0], static_cast<uint16_t>(Take(pars[1])), 16);
            break;
        case MOVD: 
            Apply(pars[0], Take(pars[1]), 32);
            break;
        case SWAP: {
                uint32_t tmp = Take(pars[1]);
                Apply(pars[1], Take(pars[0]));
                Apply(pars[0], tmp);
            }
            break;
        case OR:   
            Apply(pars[0], Take(pars[0]) | Take(pars[1])); 
            break;
        case XOR:  
            Apply(pars[0], Take(pars[0]) ^ Take(pars[1])); 
            break;
        case AND:
            Apply(pars[0], Take(pars[0]) & Take(pars[1])); 
            break;
        case SHL:
            Apply(pars[0], Take(pars[0]) << Take(pars[1])); 
            break;
        case SHR:
            Apply(pars[0], Take(pars[0]) >> Take(pars[1])); 
            break;
        case NOT:
            Apply(pars[0], ~Take(pars[0])); 
            break;
        case ADD: {
                uint64_t value = static_cast<uint64_t>(Take(pars[0])) + static_cast<uint64_t>(Take(pars[1])) + static_cast<uint64_t>(Flag(Flag::Y));
                bool y = value > 0xFFFFFFFF;
                Apply(pars[0], static_cast<uint32_t>(value));
                SetFlag(Flag::Y, y);
            }
            break;
        case SUB: {
                int64_t value = static_cast<int64_t>(Take(pars[0])) - static_cast<int64_t>(Take(pars[1])) - static_cast<int64_t>(Flag(Flag::Y));
                bool y = value < 0;
                Apply(pars[0], static_cast<uint32_t>(value));
                SetFlag(Flag::Y, y);
            }
            break;
        case CMP: {
                uint32_t p0 = Take(pars[0]), p1 = pars.size() == 1 ? p0 : Take(pars[1]);
                uint32_t diff = p0 - p1 - static_cast<uint32_t>(Flag(Flag::Y));
                SetFlag(Flag::Z, diff == 0);
                SetFlag(Flag::S, ((diff >> 31) & 1) != 0);
                SetFlag(Flag::V, false);
                SetFlag(Flag::Y, (static_cast<int64_t>(p0) - static_cast<int64_t>(p1) - static_cast<int64_t>(Flag(Flag::Y))) < 0);
                SetFlag(Flag::GT, p0 > p1);
                SetFlag(Flag::LT, p1 > p0);
            }
            break;
        case MUL: {
                uint64_t value = static_cast<uint64_t>(Take(pars[0])) * static_cast<uint64_t>(Take(pars[1]));
                bool v = value > 0xFFFFFFFF;
                Apply(pars[0], static_cast<uint32_t>(value));
                SetFlag(Flag::V, v);
            }
            break;
        case IDIV: Apply(pars[0], Take(pars[0]) / Take(pars[1])); break;
        case MOD:  Apply(pars[0], Take(pars[0]) % Take(pars[1])); break;
        case INC: {
                bool y = (static_cast<uint64_t>(Take(pars[0])) + 1) > 0xFFFFFFFF;
                Apply(pars[0], Take(pars[0])+1);
                SetFlag(Flag::Y, y);
            }
            break;
        case DEC: {
                bool y = (static_cast<int64_t>(Take(pars[0])) + 1) < 0;
                Apply(pars[0], Take(pars[0])-1);
                SetFlag(Flag::Y, y);
            }
            break;
        case BZ:
            if(Flag(Flag::Z)) { 
                PC = Take(pars[0]); 
                return; 
            }
            break;
        case BNZ:
            if(!Flag(Flag::Z)) {
                PC = Take(pars[0]);
                return;
            } 
            break;
        case BNEG:
            if(Flag(Flag::S)) {
                PC = Take(pars[0]);
                return;
            }
            break;
        case BPOS:
            if(!Flag(Flag::S)) {
                PC = Take(pars[0]);
                return;
            }
            break;
        case BGT:
            if(Flag(Flag::GT) && !Flag(Flag::Z)) {
                PC = Take(pars[0]);
                return;
            }
            break;
        case BGTE:
            if(Flag(Flag::GT) && Flag(Flag::Z)) {
                PC = Take(pars[0]);
                return;
            }
            break;
        case BLT:
            if(Flag(Flag::LT) && !Flag(Flag::Z)) {
                PC = Take(pars[0]);
                return;
            }
            break;
        case BLTE:
            if(Flag(Flag::LT) && Flag(Flag::Z)) {
                PC = Take(pars[0]);
                return;
            }
            break;
        case BV:
            if(Flag(Flag::V)) {
                PC = Take(pars[0]);
                return;
            }
            break;
        case BNV:
            if(!Flag(Flag::V)) {
                PC = Take(pars[0]);
                return;
            }
            break;
        case JMP:
            PC = Take(pars[0]);
            return;
        case JSR:
            Push32(PC + sz);
            PC = Take(pars[0]);
            return;
        case RET:
            PC = Pop32(); return;
        case PUSHB:  
            Push8(static_cast<uint8_t>(Take(pars[0]))); 
            break;
        case PUSHW:
            Push16(static_cast<uint16_t>(Take(pars[0]))); 
            break;
        case PUSHD:
            Push32(Take(pars[0])); 
            break;
        case PUSH_A:
            for(int i=0; i<=11; ++i) { 
                Push32(Register[i]);
            }
            break;
        case POPB:
            Apply(pars[0], Pop8());
            break;
        case POPW:
            Apply(pars[0], Pop16());
            break;
        case POPD:
            Apply(pars[0], Pop32());
            break;
        case POP_A:
            for(int i=11; i>=0; --i) { 
                Register[i] = Pop32();
            }
            break;
        case POPX:
            for(size_t i=0; i<Take(pars[0]); ++i) {
                Pop8();
            }
            break;
        case NOP:
            break;
        case INVALID:
        default:
            /* TODO what to do here? */
            abort();
    }

    PC += sz;
}


void CPU::Apply(Parameter const& dest, uint32_t value, uint8_t sz)
{
    SetFlag(Z, value == 0);
    SetFlag(S, ((value >> 31) & 1) != 0);
    SetFlag(V, false);
    SetFlag(Y, false);
    SetFlag(GT, false);
    SetFlag(LT, false);

    switch(dest.type) {
        case REG:
            if(dest.value >= 16) {
                abort();
            }
            Register[dest.value] = value;
            break;
        case INDREG:
            if(dest.value >= 16) {
                abort();
            }
            switch(sz) {
                case 8: 
                    _coolvm.Set(Register[dest.value], static_cast<uint8_t>(value)); 
                    break;
                case 16: 
                    _coolvm.Set16(Register[dest.value], static_cast<uint16_t>(value)); 
                    break;
                case 32:
                    _coolvm.Set32(Register[dest.value], value); 
                    break;
                default:
                    abort();
            }
            break;
        case INDV32:
            switch(sz) {
                case 8: 
                    _coolvm.Set(dest.value, static_cast<uint8_t>(value)); 
                    break;
                case 16: 
                    _coolvm.Set16(dest.value, static_cast<uint16_t>(value)); 
                    break;
                case 32:
                    _coolvm.Set32(dest.value, value); 
                    break;
                default:
                    abort();
            }
            break;
        case V8: case V16: case V32: default:
            abort();
    }
}


uint32_t CPU::Take(Parameter const& orig)
{
    switch(orig.type) {
        case NONE:                   abort();
        case REG:                    return Register[orig.value];
        case V8: case V16: case V32: return orig.value;
        case INDV32:                 return _coolvm.Get32(orig.value);
        case INDREG:                 return _coolvm.Get32(Register[orig.value]);
    }

    abort();
}


array<CPU::Parameter, 2> 
CPU::ParseParameters(InstructionInfo const& inst, uint32_t pos) const
{
    array<CPU::Parameter, 2> r;

    for (int i=0; i<2; ++i) {
        r[i].type = inst.parameters[i];
        switch (inst.parameters[i]) {
            case NONE:
                break;
            case REG: case INDREG: case V8:
                r[i].value = _coolvm.Get(pos++);
                break;
            case V16:
                r[i].value = _coolvm.Get16(pos);
                pos += 2;
                break;
            case V32: case INDV32:
                r[i].value = _coolvm.Get32(pos);
                pos += 4;
                break;
        }
    }

    if((inst.parameters[0] == REG || inst.parameters[0] == INDREG)
            && (inst.parameters[1] == REG || inst.parameters[1] == INDREG)) {
        r[1].value = r[0].value & 0xF;
        r[0].value >>= 4;
    }

    return r;
}


uint8_t
CPU::InstructionSize(InstructionInfo const& info)
{
    if ((info.parameters[0] == REG || info.parameters[0] == INDREG) 
            && (info.parameters[1] == REG || info.parameters[1] == INDREG)) {
        return 2;
    }

    uint8_t n = 1;

    for (int j=0; j<2; ++j) {
        switch(info.parameters[j]) {
            case NONE: 
                break;
            case REG: case INDREG: case V8:
                ++n; break;
            case V16:
                n += 2; break;
            case V32: case INDV32:
                n += 4; break;
        }
    }

    return n;
}


uint8_t 
CPU::InstructionSize(uint8_t i)
{
    return InstructionSize(instructions[i]);
}


// }}}

// {{{ FLAGS

bool CPU::Flag(enum Flag f) const
{
    return static_cast<bool>((FL >> static_cast<int>(f)) & 1);
}


void
CPU::SetFlag(enum Flag f, bool value)
{
    int64_t new_value = FL;
    new_value ^= (-static_cast<int>(value) ^ new_value) & (1 << static_cast<int>(f));
    FL = static_cast<uint32_t>(new_value);
}

// }}}

// {{{ STACK

void CPU::Push8(uint8_t value)
{
    _coolvm.Set(SP, value);
    SP -= 1;
}

void CPU::Push16(uint16_t value)
{
    SP -= 1;
    _coolvm.Set16(SP, value);
    SP -= 1;
}

void CPU::Push32(uint32_t value)
{
    SP -= 3;
    _coolvm.Set32(SP, value);
    SP -= 1;
}

uint8_t CPU::Pop8() {
    SP += 1;
    return _coolvm.Get(SP);
}

uint16_t CPU::Pop16() {
    SP += 1;
    uint16_t value = _coolvm.Get16(SP);
    SP += 1;
    return value;
}

uint32_t CPU::Pop32() {
    SP += 1;
    uint32_t value = _coolvm.Get32(SP);
    SP += 3;
    return value;
}

// }}}

// {{{ INTERRUPTS

uint32_t 
CPU::InterruptPointer(Interrupt interrupt) const
{
    return _coolvm.Get32(_coolvm.Get32(CPUINT) + (interrupt * 4));
}

void
CPU::SetInterruptPointer(Interrupt interrupt, uint32_t address)
{
    _coolvm.Set32(_coolvm.Get32(CPUINT) + (interrupt * 4), address);
}

bool
CPU::InterruptFired(Interrupt interrupt) const
{
    return _interrupt_fired[interrupt];
}
    
void 
CPU::SetInterruptFired(Interrupt interrupt, bool value)
{
    _interrupt_fired[interrupt] = value;
}

// }}}

// {{{ DEBUG INSTRUCTION

CPU::DbgInstruction 
CPU::DebugInstruction(uint32_t pos) const
{
    uint8_t opcode = _coolvm.Get(pos++);
    InstructionInfo const& ii = instructions[opcode];

    stringstream ss;
    string data = instruction_name[ii.instruction];
    transform(data.begin(), data.end(), data.begin(), ::tolower);
    ss << data;

    auto regname = [](uint8_t r) {
        if (r > 0xF) {
            return "??";
        } else {
            return register_name[r];
        }
    };
    
    if ((ii.parameters[0] == REG || ii.parameters[0] == INDREG) && (ii.parameters[1] == REG || ii.parameters[1] == INDREG)) {
        uint8_t p = _coolvm.Get(pos);
        for (int j=0; j<2; ++j) {
            if (j == 1) { 
                ss << ",";
            }
            switch(ii.parameters[j]) {
                case REG:
                    ss << " " << regname(p & 0xF);
                    break;
                case INDREG: 
                    ss << " [" << regname(p & 0xF) << "]";
                    break;
                default:
                    abort();
            }
            p <<= 4;
        }
    } else {
        for (int j=0; j<2; ++j) {
            if (j == 1 && ii.parameters[j] != NONE) { 
                ss << ",";
            }
            switch(ii.parameters[j]) {
                case NONE:
                    break;
                case REG:
                    ss << " " << regname(_coolvm.Get(pos++) & 0xF);
                    break;
                case INDREG: 
                    ss << " [" << regname(_coolvm.Get(pos++) & 0xF) << "]";
                    break;
                case V8:
                    ss << " 0x" << uppercase << hex << static_cast<int>(_coolvm.Get(pos++));
                    break;
                case V16:
                    ss << " 0x" << uppercase << hex << _coolvm.Get16(pos++);
                    break;
                case V32:
                    ss << " 0x" << uppercase << hex << _coolvm.Get32(pos++);
                    break;
                case INDV32:
                    ss << " [0x" << uppercase << hex << _coolvm.Get32(pos++) << "]";
                    break;
            }
        }
    }

    return CPU::DbgInstruction { ss.str(), InstructionSize(opcode) };
}

// }}}

// {{{ C INTERFACE

extern "C" {

    uint32_t _coolvm_cpu_get_register(CoolVM* cvm, uint8_t n)
    {
        assert(n < 16);
        return cvm->cpu.Register[n];
    }


    void _coolvm_cpu_set_register(CoolVM* cvm, uint8_t n, uint32_t value)
    {
        assert(n < 16);
        cvm->cpu.Register[n] = value;
    }


    uint8_t _coolvm_cpu_debug_instruction(CoolVM* cvm, uint32_t pos, char inst[30])
    {
        CPU::DbgInstruction i = cvm->cpu.DebugInstruction(pos);
        snprintf(inst, 30, "%s", i.description.c_str());
        return i.sz;
    }
}

// }}}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
