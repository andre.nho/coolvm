#ifndef COOLVM_CPU_HH_
#define COOLVM_CPU_HH_

#include <cstdint>
#include <array>
#include <vector>
using namespace std;

// {{{ INSTRUCTIONS

#define LIST_OF_INSTRUCTIONS \
    X(INVALID) \
    X(ADD) \
    X(AND) \
    X(BGT) \
    X(BGTE) \
    X(BLT) \
    X(BLTE) \
    X(BNEG) \
    X(BNV) \
    X(BNZ) \
    X(BPOS) \
    X(BV) \
    X(BZ) \
    X(CMP) \
    X(DBG) \
    X(DEC) \
    X(IDIV) \
    X(INC) \
    X(JMP) \
    X(JSR) \
    X(MOD) \
    X(MOV) \
    X(MOVB) \
    X(MOVD) \
    X(MOVW) \
    X(MUL) \
    X(NOP) \
    X(NOT) \
    X(OR) \
    X(POP_A) \
    X(POPB) \
    X(POPD) \
    X(POPW) \
    X(POPX) \
    X(PUSH_A) \
    X(PUSHB) \
    X(PUSHD) \
    X(PUSHW) \
    X(RET) \
    X(SHL) \
    X(SHR) \
    X(SUB) \
    X(SWAP) \
    X(XOR)

#define X(name) name,
enum Instruction { LIST_OF_INSTRUCTIONS };
#undef X

enum InstructionParameter { NONE, REG, V8, V16, V32, INDREG, INDV32 };

struct InstructionInfo {
    Instruction instruction;
    array<InstructionParameter, 2> parameters;

    constexpr InstructionInfo() noexcept : InstructionInfo(INVALID) {}
    constexpr InstructionInfo(Instruction instruction, InstructionParameter parameter1=NONE, InstructionParameter parameter2=NONE) noexcept
        : instruction(instruction), parameters({ parameter1, parameter2 }) {}
};

// }}}

enum Flag { Y, V, Z, S, GT, LT };

class CPU {
public:
    CPU(class CoolVM& coolvm);

    void Step();

    struct DbgInstruction { string description; uint8_t sz; };
    DbgInstruction DebugInstruction(uint32_t pos) const;

    array<uint32_t, 16> Register = { 0 };

    bool Flag(enum Flag f) const;
    void SetFlag(enum Flag f, bool value);

    // register aliases
    uint32_t& SP = Register[0xD],
            & PC = Register[0xE],
            & FL = Register[0xF];

    // interrupts
    enum Interrupt { 
        IINVOP,
        ISCREEN, ITIMA, ITIMB, IKEY, IJOY, IMOUMOV, IMOUBTN, 
        IDRIVE0, IDRIVE1, IDRIVE2, IDRIVE3,
        INTERRUPT_COUNT
    };
    uint32_t InterruptPointer(Interrupt interrupt) const;
    void     SetInterruptPointer(Interrupt interrupt, uint32_t address);
    bool InterruptFired(Interrupt interrupt) const;
    void SetInterruptFired(Interrupt interrupt, bool value=true);

private:  // {{{
    struct Parameter {
        InstructionParameter type;
        uint32_t             value;
        Parameter() : type(NONE), value(0) {}
    };

    class CoolVM& _coolvm;

    static const uint32_t VERSION = 1;

    array<bool, INTERRUPT_COUNT> _interrupt_fired = { false };

    static uint8_t InstructionSize(uint8_t i);
    static uint8_t InstructionSize(InstructionInfo const& info);

    array<Parameter, 2> ParseParameters(InstructionInfo const& inst, uint32_t pos) const;

    void     Apply(CPU::Parameter const& dest, uint32_t value, uint8_t sz=0);
    uint32_t Take(CPU::Parameter const& orig);

    void Push8(uint8_t value);
    void Push16(uint16_t value);
    void Push32(uint32_t value);
    uint8_t Pop8();
    uint16_t Pop16();
    uint32_t Pop32();

#define X(name) #name,
    static constexpr const char* instruction_name[] = { LIST_OF_INSTRUCTIONS };
#undef X
    static constexpr const char* register_name[] = {
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "FP", "SP", "PC", "FL"
    };

    static constexpr InstructionInfo instructions[256] = {  // {{{
        /* 0x00 */ { INVALID },
        /* 0x01 */ { MOV, REG, REG },
        /* 0x02 */ { MOV, REG, V8 },
        /* 0x03 */ { MOV, REG, V16 },
        /* 0x04 */ { MOV, REG, V32 },
        /* 0x05 */ { MOVB, REG, INDREG },
        /* 0x06 */ { MOVB, REG, INDV32 },
        /* 0x07 */ { MOVB, INDREG, REG },
        /* 0x08 */ { MOVB, INDREG, V8 },
        /* 0x09 */ { MOVB, INDREG, INDREG },
        /* 0x0A */ { MOVB, INDREG, INDV32 },
        /* 0x0B */ { MOVB, INDV32, REG },
        /* 0x0C */ { MOVB, INDV32, V8 },
        /* 0x0D */ { MOVB, INDV32, INDREG },
        /* 0x0E */ { MOVB, INDV32, INDV32 },
        /* 0x0F */ { MOVW, REG, INDREG },
        /* 0x10 */ { MOVW, REG, INDV32 },
        /* 0x11 */ { MOVW, INDREG, REG },
        /* 0x12 */ { MOVW, INDREG, V16 },
        /* 0x13 */ { MOVW, INDREG, INDREG },
        /* 0x14 */ { MOVW, INDREG, INDV32 },
        /* 0x15 */ { MOVW, INDV32, REG },
        /* 0x16 */ { MOVW, INDV32, V16 },
        /* 0x17 */ { MOVW, INDV32, INDREG },
        /* 0x18 */ { MOVW, INDV32, INDV32 },
        /* 0x19 */ { MOVD, REG, INDREG },
        /* 0x1A */ { MOVD, REG, INDV32 },
        /* 0x1B */ { MOVD, INDREG, REG },
        /* 0x1C */ { MOVD, INDREG, V32 },
        /* 0x1D */ { MOVD, INDREG, INDREG },
        /* 0x1E */ { MOVD, INDREG, INDV32 },
        /* 0x1F */ { MOVD, INDV32, REG },
        /* 0x20 */ { MOVD, INDV32, V32 },
        /* 0x21 */ { MOVD, INDV32, INDREG },
        /* 0x22 */ { MOVD, INDV32, INDV32 },
        /* 0x23 */ { SWAP, REG, REG },
        /* 0x24 */ { OR, REG, REG },
        /* 0x25 */ { OR, REG, V8 },
        /* 0x26 */ { OR, REG, V16 },
        /* 0x27 */ { OR, REG, V32 },
        /* 0x28 */ { XOR, REG, REG },
        /* 0x29 */ { XOR, REG, V8 },
        /* 0x2A */ { XOR, REG, V16 },
        /* 0x2B */ { XOR, REG, V32 },
        /* 0x2C */ { AND, REG, REG },
        /* 0x2D */ { AND, REG, V8 },
        /* 0x2E */ { AND, REG, V16 },
        /* 0x2F */ { AND, REG, V32 },
        /* 0x30 */ { SHL, REG, REG },
        /* 0x31 */ { SHL, REG, V8 },
        /* 0x32 */ { SHR, REG, REG },
        /* 0x33 */ { SHR, REG, V8 },
        /* 0x34 */ { NOT, REG, NONE },
        /* 0x35 */ { ADD, REG, REG },
        /* 0x36 */ { ADD, REG, V8 },
        /* 0x37 */ { ADD, REG, V16 },
        /* 0x38 */ { ADD, REG, V32 },
        /* 0x39 */ { SUB, REG, REG },
        /* 0x3A */ { SUB, REG, V8 },
        /* 0x3B */ { SUB, REG, V16 },
        /* 0x3C */ { SUB, REG, V32 },
        /* 0x3D */ { CMP, REG, REG },
        /* 0x3E */ { CMP, REG, V8 },
        /* 0x3F */ { CMP, REG, V16 },
        /* 0x40 */ { CMP, REG, V32 },
        /* 0x41 */ { CMP, REG, NONE },
        /* 0x42 */ { MUL, REG, REG },
        /* 0x43 */ { MUL, REG, V8 },
        /* 0x44 */ { MUL, REG, V16 },
        /* 0x45 */ { MUL, REG, V32 },
        /* 0x46 */ { IDIV, REG, REG },
        /* 0x47 */ { IDIV, REG, V8 },
        /* 0x48 */ { IDIV, REG, V16 },
        /* 0x49 */ { IDIV, REG, V32 },
        /* 0x4A */ { MOD, REG, REG },
        /* 0x4B */ { MOD, REG, V8 },
        /* 0x4C */ { MOD, REG, V16 },
        /* 0x4D */ { MOD, REG, V32 },
        /* 0x4E */ { INC, REG, NONE },
        /* 0x4F */ { DEC, REG, NONE },
        /* 0x50 */ { BZ, REG, NONE },
        /* 0x51 */ { BZ, V32, NONE },
        /* 0x52 */ { BNZ, REG, NONE },
        /* 0x53 */ { BNZ, V32, NONE },
        /* 0x54 */ { BNEG, REG, NONE },
        /* 0x55 */ { BNEG, V32, NONE },
        /* 0x56 */ { BPOS, REG, NONE },
        /* 0x57 */ { BPOS, V32, NONE },
        /* 0x58 */ { BGT, REG, NONE },
        /* 0x59 */ { BGT, V32, NONE },
        /* 0x5A */ { BGTE, REG, NONE },
        /* 0x5B */ { BGTE, V32, NONE },
        /* 0x5C */ { BLT, REG, NONE },
        /* 0x5D */ { BLT, V32, NONE },
        /* 0x5E */ { BLTE, REG, NONE },
        /* 0x5F */ { BLTE, V32, NONE },
        /* 0x60 */ { BV, REG, NONE },
        /* 0x61 */ { BV, V32, NONE },
        /* 0x62 */ { BNV, REG, NONE },
        /* 0x63 */ { BNV, V32, NONE },
        /* 0x64 */ { JMP, REG, NONE },
        /* 0x65 */ { JMP, V32, NONE },
        /* 0x66 */ { JSR, REG, NONE },
        /* 0x67 */ { JSR, V32, NONE },
        /* 0x68 */ { RET, REG, NONE },
        /* 0x69 */ { PUSHB, REG, NONE },
        /* 0x6A */ { PUSHB, V8, NONE },
        /* 0x6B */ { PUSHW, REG, NONE },
        /* 0x6C */ { PUSHW, V16, NONE },
        /* 0x6D */ { PUSHD, REG, NONE },
        /* 0x6E */ { PUSHD, V32, NONE },
        /* 0x6F */ { PUSH_A, NONE, NONE },
        /* 0x70 */ { POPB, REG, NONE },
        /* 0x71 */ { POPW, REG, NONE },
        /* 0x72 */ { POPD, REG, NONE },
        /* 0x73 */ { POP_A, NONE, NONE },
        /* 0x74 */ { POPX, REG, NONE },
        /* 0x75 */ { POPX, V8, NONE },
        /* 0x76 */ { POPX, V16, NONE },
        /* 0x77 */ { NOP, NONE, NONE },
        /* 0x78 */ { DBG, NONE, NONE },
    };  // }}}
    // }}}
};

#endif

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
