Source organization
===================

    coolvm/lib         CoolVM C library
    coolvm/py          CoolVM library - Python bindings
    coolvm/dbg         CoolVM desktop debugger
    coolvm/js          CoolVM javascript emulator


Memory organization
===================

The memory map contains two fixed areas:

    0000..7BFF   BIOS 
    7C00..7FFF   Hardware registers

Other than that, the memory is viewed as a flat RAM.

The minimum amount of memory supported is 48 kB (32 kB are used by the BIOS and registers).

The rest of the memory map is defined by the hardware registers. The registers 
contain pointers to memory areas. The hardware registers are defined in the next
sections.

The memory size is custom defined. This information is avaliable in the
register `MEMSZ`. The maximum amount of memory is 4 GB. The default memory
size is 4 Mb.
    

BIOS
====

The BIOS has a maximum of 31 kB, and is placed in the position 0x0 of the memory.
When the computer is turned on, the BIOS is the first thing loaded into memory.

The BIOS code does the following:

1. Prepare sprites for writing text on the screen
2. Shows a logo
3. Initialize devices, informing the process on the screen
4. Tries to load a kernel from disk


CPU
===

The register `CPUCFG` contains a struct with the following configuration for the CPU:

```c
struct CPUCFG {
    uint8_t  version :  8;
    uint32_t unused  : 24;
};
```

Opcodes
-------

The CPU is little-endian and has 16 32-bit registers:

<table>
    <tr><th>Number</th><th>Register</th><th>Pourpose</th></tr>
    <tr><td><code>0x0</code></td><td><code>A</code></td><td rowspan="12">General pourpose registers</td></tr>
    <tr><td><code>0x1</code></td><td><code>B</code></td></tr>
    <tr><td><code>0x2</code></td><td><code>C</code></td></tr>
    <tr><td><code>0x3</code></td><td><code>D</code></td></tr>
    <tr><td><code>0x4</code></td><td><code>E</code></td></tr>
    <tr><td><code>0x5</code></td><td><code>F</code></td></tr>
    <tr><td><code>0x6</code></td><td><code>G</code></td></tr>
    <tr><td><code>0x7</code></td><td><code>H</code></td></tr>
    <tr><td><code>0x8</code></td><td><code>I</code></td></tr>
    <tr><td><code>0x9</code></td><td><code>J</code></td></tr>
    <tr><td><code>0xA</code></td><td><code>K</code></td></tr>
    <tr><td><code>0xB</code></td><td><code>L</code></td></tr>
    <tr><td><code>0xC</code></td><td><code>FP</code></td><td>Frame pointer (used for recursive functions)</td></tr>
    <tr><td><code>0xD</code></td><td><code>SP</code></td><td>Stack pointer (points to the top of the stack)</td></tr>
    <tr><td><code>0xE</code></td><td><code>PC</code></td><td>Program counter (points to the next instruction)</td></tr>
    <tr><td><code>0xF</code></td><td><code>FL</code></td><td>Flags (see below)</td></tr>
</table>

The CPU architecture is orthogonal - that is, any instruction can operate on any register.

The `FL` register contains a set of flags that are affected by different
operations. Each flag in one bit in the register:

<table>
    <tr><th>Bit</th><th>Flag</th><th>Event</th></tr>
    <tr><td><code>0</code></td><td><code>Y</code></td><td>Last instruction caused a <i>carry</i></td></tr>
    <tr><td><code>1</code></td><td><code>V</code></td><td>Last instruction caused a <i>overflow</i></td></tr>
    <tr><td><code>2</code></td><td><code>Z</code></td><td>Last instruction's result was zero</td></tr>
    <tr><td><code>3</code></td><td><code>S</code></td><td>Last instruction's result was under zero (that is: the 31th bit was set)</td></tr>
    <tr><td><code>4</code></td><td><code>GT</code></td><td>Last comparison instruction was <i>greater than</i></td></tr>
    <tr><td><code>5</code></td><td><code>LT</code></td><td>Last comparison instruction was <i>less than</i></td></tr>
</table>

Each instruction in memory is formed by the instruction itself (first byte) 
and the parameters. The parameters can go from 1 to 4 bytes in length, and there
are at most two parameters per instruction. Therefore, a instruction can be from
1 to 9 bytes in length.

The parameters can be one of the following types:

<ul>
    <li><i>reg</i> - an register. In this case, the register number from the 
        register table above is used. If the two parameters are registers,
        one single byte is used for both. Example: <code>mov B, C</code> copies
        the value in the register <code>C</code> to the register <code>B</code>;
    <li><i>v8</i>, <i>v16</i> or <i>v16</i> - A straight number, that can be
        8, 16 or 32 bits. For example, the instruction <code>add J, 0x1234</code>
        will sum <code>0x1234</code> to the register <code>J</code>;
    <li><i>indreg</i> - returs the value in the memory position of the value
        stored in the register. The register is represented in brackets. For 
        example, if the register <code>K</code> contains <code>0xABCD</code>,
        the instruction <code>mov A, [K]</code> will set the value of the
        register <code>A</code> to the value stored in memory position
        <code>0xABCD</code>;
    <li><i>indv32</i> - this is the same as <i>indreg</i>, but it returns the
        value in a declared memory position. For example, <code>mov8 F, [0x1234]</code>
        will get the value in memory position <code>0x1234</code> and copy it
        to the register <code>F</code>.
</ul>

The table below presents the instruction set:

<table>
    <tr><th>Group</th><th>Opcode</th><th>Instruction</th><th>Parameters</th><th>Description</th><th>Pseudocode</th></tr>
    <tr>
        <td rowspan="35">Movement</td>
        <td><code>0x01</code></td>
        <td rowspan="4"><code>mov</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="4">Moves data from one location to the other</td>
        <td rowspan="4"><code>dest = origin</code></td>
    </tr>
    <tr><td><code>0x02</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x03</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x04</code></td><td><i>reg</i>, <i>v32</i></td></tr>

    <tr>
        <td><code>0x05</code></td>
        <td rowspan="10"><code>movb</code></td>
        <td><i>reg</i>, <i>indreg</i></td>
        <td rowspan="10">Moves 8-bit data from one location to the other</td>
        <td rowspan="10"><code>dest = (8bit)origin</code></td>
    </tr>
    <tr><td><code>0x06</code></td><td><i>reg</i>, <i>indv32</i></td></tr>
    <tr><td><code>0x07</code></td><td><i>indreg</i>, <i>reg</i></td></tr>
    <tr><td><code>0x08</code></td><td><i>indreg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x09</code></td><td><i>indreg</i>, <i>indreg</i></td></tr>
    <tr><td><code>0x0A</code></td><td><i>indreg</i>, <i>indv32</i></td></tr>
    <tr><td><code>0x0B</code></td><td><i>indv32</i>, <i>reg</i></td></tr>
    <tr><td><code>0x0C</code></td><td><i>indv32</i>, <i>v8</i></td></tr>
    <tr><td><code>0x0D</code></td><td><i>indv32</i>, <i>indreg</i></td></tr>
    <tr><td><code>0x0E</code></td><td><i>indv32</i>, <i>indv32</i></td></tr>

    <tr>
        <td><code>0x0F</code></td>
        <td rowspan="10"><code>movw</code></td>
        <td><i>reg</i>, <i>indreg</i></td>
        <td rowspan="10">Moves 16-bit data from one location to the other</td>
        <td rowspan="10"><code>dest = (16bit)origin</code></td>
    </tr>
    <tr><td><code>0x10</code></td><td><i>reg</i>, <i>indv32</i></td></tr>
    <tr><td><code>0x11</code></td><td><i>indreg</i>, <i>reg</i></td></tr>
    <tr><td><code>0x12</code></td><td><i>indreg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x13</code></td><td><i>indreg</i>, <i>indreg</i></td></tr>
    <tr><td><code>0x14</code></td><td><i>indreg</i>, <i>indv32</i></td></tr>
    <tr><td><code>0x15</code></td><td><i>indv32</i>, <i>reg</i></td></tr>
    <tr><td><code>0x16</code></td><td><i>indv32</i>, <i>v16</i></td></tr>
    <tr><td><code>0x17</code></td><td><i>indv32</i>, <i>indreg</i></td></tr>
    <tr><td><code>0x18</code></td><td><i>indv32</i>, <i>indv32</i></td></tr>

    <tr>
        <td><code>0x19</code></td>
        <td rowspan="10"><code>movd</code></td>
        <td><i>reg</i>, <i>indreg</i></td>
        <td rowspan="10">Moves 32-bit data from one location to the other</td>
        <td rowspan="10"><code>dest = (32bit)origin</code></td>
    </tr>
    <tr><td><code>0x1A</code></td><td><i>reg</i>, <i>indv32</i></td></tr>
    <tr><td><code>0x1B</code></td><td><i>indreg</i>, <i>reg</i></td></tr>
    <tr><td><code>0x1C</code></td><td><i>indreg</i>, <i>v32</i></td></tr>
    <tr><td><code>0x1D</code></td><td><i>indreg</i>, <i>indreg</i></td></tr>
    <tr><td><code>0x1E</code></td><td><i>indreg</i>, <i>indv32</i></td></tr>
    <tr><td><code>0x1F</code></td><td><i>indv32</i>, <i>reg</i></td></tr>
    <tr><td><code>0x20</code></td><td><i>indv32</i>, <i>v32</i></td></tr>
    <tr><td><code>0x21</code></td><td><i>indv32</i>, <i>indreg</i></td></tr>
    <tr><td><code>0x22</code></td><td><i>indv32</i>, <i>indv32</i></td></tr>

    <tr>
        <td><code>0x23</code></td>
        <td><code>swap</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td>Swap the values of the registers</td>
        <td><code>t = par1; par1 = par2; par2 = t</code></td>
    </tr>

    <tr>
        <td rowspan="17">Logic</td>
        <td><code>0x24</code></td>
        <td rowspan="4"><code>or</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="4">Logical OR instruction</td>
        <td rowspan="4"><code>dest |= origin</code></td>
    </tr>
    <tr><td><code>0x25</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x26</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x27</code></td><td><i>reg</i>, <i>v32</i></td></tr>

    <tr>
        <td><code>0x28</code></td>
        <td rowspan="4"><code>xor</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="4">Logical XOR instruction</td>
        <td rowspan="4"><code>dest ^= origin</code></td>
    </tr>
    <tr><td><code>0x29</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x2A</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x2B</code></td><td><i>reg</i>, <i>v32</i></td></tr>

    <tr>
        <td><code>0x2C</code></td>
        <td rowspan="4"><code>and</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="4">Logical AND instruction</td>
        <td rowspan="4"><code>dest &amp;= origin</code></td>
    </tr>
    <tr><td><code>0x2D</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x2E</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x2F</code></td><td><i>reg</i>, <i>v32</i></td></tr>

    <tr>
        <td><code>0x30</code></td>
        <td rowspan="2"><code>shl</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="2">Shift <i>n</i> bytes left</td>
        <td rowspan="2"><code>dest &lt;&lt;= origin</code></td>
    </tr>
    <tr><td><code>0x31</code></td><td><i>reg</i>, <i>v8</i></td></tr>

    <tr>
        <td><code>0x32</code></td>
        <td rowspan="2"><code>shl</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="2">Shift <i>n</i> bytes right</td>
        <td rowspan="2"><code>dest &gt;&gt;= origin</code></td>
    </tr>
    <tr><td><code>0x33</code></td><td><i>reg</i>, <i>v8</i></td></tr>

    <tr>
        <td><code>0x34</code></td>
        <td><code>not</code></td>
        <td><i>reg</i></td>
        <td>Invert the value in the registers (NOT operator)</td>
        <td><code>par = ~par</code></td>
    </tr>

    <tr>
        <td rowspan="27">Arithmetic</td>
        <td><code>0x35</code></td>
        <td rowspan="4"><code>add</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="4">Arithmetic sum. Set the flag <code>Y</code> in case of carry.</td>
        <td rowspan="4"><code>dest += origin + Y</code></td>
    </tr>
    <tr><td><code>0x36</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x37</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x38</code></td><td><i>reg</i>, <i>v32</i></td></tr>

    <tr>
        <td><code>0x39</code></td>
        <td rowspan="4"><code>sub</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="4">Arithmetic subtraction. Set the flag <code>Y</code> in case of carry.</td>
        <td rowspan="4"><code>dest -= origin + Y</code></td>
    </tr>
    <tr><td><code>0x3A</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x3B</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x3C</code></td><td><i>reg</i>, <i>v32</i></td></tr>

    <tr>
        <td><code>0x3D</code></td>
        <td rowspan="5"><code>cmp</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="5">Perform a subtraction, but does not set the result, only the flags. This is the only instruction that sets the flags <code>GT</code> and <code>LT</code>.</td>
        <td rowspan="5"><code>[flags] = dest - origin</code></td>
    </tr>
    <tr><td><code>0x3E</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x3F</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x40</code></td><td><i>reg</i>, <i>v32</i></td></tr>
    <tr><td><code>0x41</code></td><td><i>reg</i></td></tr>

    <tr>
        <td><code>0x42</code></td>
        <td rowspan="4"><code>mul</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="4">Multiply two numbers. Sets the <code>V</code> flag (overflow) it the result is above 32 bits.</td>
        <td rowspan="4"><code>dest *= origin</code></td>
    </tr>
    <tr><td><code>0x43</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x44</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x45</code></td><td><i>reg</i>, <i>v32</i></td></tr>

    <tr>
        <td><code>0x46</code></td>
        <td rowspan="4"><code>idiv</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="4">Perfoms an integer division.</td>
        <td rowspan="4"><code>dest //= origin</code></td>
    </tr>
    <tr><td><code>0x47</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x48</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x49</code></td><td><i>reg</i>, <i>v32</i></td></tr>

    <tr>
        <td><code>0x4A</code></td>
        <td rowspan="4"><code>mod</code></td>
        <td><i>reg</i>, <i>reg</i></td>
        <td rowspan="4">Calculate the modulo of two numbers.</td>
        <td rowspan="4"><code>dest %= origin</code></td>
    </tr>
    <tr><td><code>0x4B</code></td><td><i>reg</i>, <i>v8</i></td></tr>
    <tr><td><code>0x4C</code></td><td><i>reg</i>, <i>v16</i></td></tr>
    <tr><td><code>0x4D</code></td><td><i>reg</i>, <i>v32</i></td></tr>

    <tr>
        <td><code>0x4E</code></td>
        <td><code>inc</code></td>
        <td><i>reg</i></td>
        <td>Increment the value in the register by one</td>
        <td><code>++par</code></td>
    </tr>

    <tr>
        <td><code>0x4F</code></td>
        <td><code>dec</code></td>
        <td><i>reg</i></td>
        <td>Decrement the value in the register by one</td>
        <td><code>--par</code></td>
    </tr>

    <tr>
        <td rowspan="25">Branches</td>
        <td><code>0x50</code></td>
        <td rowspan="2"><code>bz</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>Z</code> flag is set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (Z) PC = par</code></td>
    </tr>
    <tr><td><code>0x51</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x52</code></td>
        <td rowspan="2"><code>bnz</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>Z</code> flag is not set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (!Z) PC = par</code></td>
    </tr>
    <tr><td><code>0x53</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x54</code></td>
        <td rowspan="2"><code>bneg</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>S</code> flag is set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (S) PC = par</code></td>
    </tr>
    <tr><td><code>0x55</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x56</code></td>
        <td rowspan="2"><code>bpos</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>S</code> flag is not set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (!S) PC = par</code></td>
    </tr>
    <tr><td><code>0x57</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x58</code></td>
        <td rowspan="2"><code>bgt</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>GT</code> flag is set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (GT) PC = par</code></td>
    </tr>
    <tr><td><code>0x59</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x5A</code></td>
        <td rowspan="2"><code>bgte</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>GT</code> and <code>Z</code> flag is set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (GT &amp;&amp; Z) PC = par</code></td>
    </tr>
    <tr><td><code>0x5B</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x5C</code></td>
        <td rowspan="2"><code>blt</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>LT</code> flag is set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (LT) PC = par</code></td>
    </tr>
    <tr><td><code>0x5D</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x5E</code></td>
        <td rowspan="2"><code>blte</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>LT</code> and <code>Z</code> flag is set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (LT &amp;&amp; Z) PC = par</code></td>
    </tr>
    <tr><td><code>0x5F</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x60</code></td>
        <td rowspan="2"><code>bv</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>V</code> flag is set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (V) PC = par</code></td>
    </tr>
    <tr><td><code>0x61</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x62</code></td>
        <td rowspan="2"><code>bnv</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">If <code>V</code> flag is not set, jump to instuction in parameter.</td>
        <td rowspan="2"><code>if (!V) PC = par</code></td>
    </tr>
    <tr><td><code>0x63</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x64</code></td>
        <td rowspan="2"><code>jmp</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">Unconditionally jumps to instruction in parameter.</td>
        <td rowspan="2"><code>PC = par</code></td>
    </tr>
    <tr><td><code>0x65</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x66</code></td>
        <td rowspan="2"><code>jsr</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">Jump to subroutine: stores the next instruction in the stack, and jump to location</td>
        <td rowspan="2"><code>push(PC); PC = par</code></td>
    </tr>
    <tr><td><code>0x67</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x68</code></td>
        <td><code>ret</code></td>
        <td><i>reg</i></td>
        <td>Return from subroutine: pops value from stack and jump to it</td>
        <td><code>PC = pop()</code></td>
    </tr>

    <tr>
        <td rowspan="14">Stack operations</td>
        <td><code>0x6A</code></td>
        <td rowspan="2"><code>pushb</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">Push one byte into the stack</td>
        <td rowspan="2"><code>[SP] = par; SP -= 1</code>
    </tr>
    <tr><td><code>0x6B</code></td><td><i>v8</i></td></tr>

    <tr>
        <td><code>0x6C</code></td>
        <td rowspan="2"><code>pushw</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">Push one word (two bytes) into the stack</td>
        <td rowspan="2"><code>[SP] = par; SP -= 2</code>
    </tr>
    <tr><td><code>0x6D</code></td><td><i>v16</i></td></tr>

    <tr>
        <td><code>0x6E</code></td>
        <td rowspan="2"><code>pushd</code></td>
        <td><i>reg</i></td>
        <td rowspan="2">Push one double-word (four bytes) into the stack</td>
        <td rowspan="2"><code>[SP] = par; SP -= 4</code>
    </tr>
    <tr><td><code>0x6F</code></td><td><i>v32</i></td></tr>

    <tr>
        <td><code>0x70</code></td>
        <td><code>push.a</code></td>
        <td></td>
        <td>Push all registers (except FP, SP, PC and FL) into the stack</td>
        <td><code>for(i = 0..11) push(reg[i])</code></td>
    </tr>

    <tr>
        <td><code>0x71</code></td>
        <td><code>popb</code></td>
        <td><i>reg</i></td>
        <td>Pop one byte from the stack, and store it in the register</td>
        <td><code>par = [SP]; SP += 1</code></td>
    </tr>

    <tr>
        <td><code>0x72</code></td>
        <td><code>popw</code></td>
        <td><i>reg</i></td>
        <td>Pop one word (two bytes) from the stack, and store it in the register</td>
        <td><code>par = [SP]; SP += 2</code></td>
    </tr>

    <tr>
        <td><code>0x73</code></td>
        <td><code>popd</code></td>
        <td><i>reg</i></td>
        <td>Pop one double-word (four bytes) from the stack, and store it in the register</td>
        <td><code>par = [SP]; SP += 4</code></td>
    </tr>

    <tr>
        <td><code>0x74</code></td>
        <td><code>pop.a</code></td>
        <td></td>
        <td>Pop all registers (except FP, SP, PC and FL) from the stack</td>
        <td><code>for(i = 11..0) reg[i] = pop()</code></td>
    </tr>

    <tr>
        <td><code>0x75</code></td>
        <td rowspan="3"><code>popx</code></td>
        <td><i>reg</i></td>
        <td rowspan="3">Pop <i>n</i> bytes from the stack, discarding them</td>
        <td rowspan="3"><code>SP += 4</code>
    </tr>
    <tr><td><code>0x76</code></td><td><i>v8</i></td></tr>
    <tr><td><code>0x77</code></td><td><i>v16</i></td></tr>
    
    <tr>
        <td rowspan="2">Others</td>
        <td><code>0x78</code></td>
        <td><code>nop</code></td>
        <td></td>
        <td>Does nothing for one cycle</td>
        <td><code>/* do nothing */</code></td>
    </tr>

    <tr>
        <td><code>0x7A</code></td>
        <td><code>dbg</code></td>
        <td></td>
        <td>Enter the debugger. Use as a breakpoint for debugging applications.</td>
        <td><code>debugger()</code></td>
    </tr>

</table>

If an invalid opcode is called, the interrupt `IINVOP` is fired.

Interrupts
----------

An interrupt table is avaliable at the position indicated by the register `CPUINT`. There can be up to 16 interrupts.


Video
=====

The video resolution is 320x240.

The video can display 256 colors simultaneously.  The color of each byte value comes from a palette, which is pointed by the register `VIDPAL` (768 bytes). 
The color 255 is transparent for the tile and sprite layers.

Each frame, four layers are drawn:

  * the __background__, which is a pixmap;
  * the __tiles__, which is based on a tilemap of 16x16 tiles, placed on a grid;
  * the __sprites__, which is based in a spritemap of 16x16 sprites, which can be placed anywhere on the 
    screen with different priorities;
  * the __ui__, that is the same as the tiles layer, but with higher priority. It is generaly used to draw UI elements.

The `VIDCFG` register is a 32-bit struct that contains information pertinent to video:

```c
struct VIDCFG {
    uint8_t version;    /* video adapter version */
    uint8_t bg_color;   /* background color - if 0xFF, use the image pointer by VIDBG */
    uint8_t border_color;
    uint8_t bg_size   : 1;  /* 0 = 320x240, 1 = 640x480 */
};
```

The screen is redrawn at 60 fps, which takes 1 million CPU clocks.

Background layer
----------------

The background layer can be a single color (if the background color in `VIDCFG` is different from 0xFF). In this
case, a simple flat background is drawn.

If `VIDCFG` is equal to 0xFF, then the background drawn is the one pointed by the register `VIDBKG`. The background 
is a flat binary block of pixels, where each byte is equal to one pixel color (as defined in the palette).

The background is 320x240, or double if the `bg_size` bit is set on `VIDCFG`. Since the screen is smaller than the background, 
the register `VIDBKRL` can be used to set the relative position of the image on the screen. The format of this register is

```c
struct VIDBKRL {
    uint16_t disp_x;
    uint16_t disp_y;
}
```

If the displacement is larger than the screen, the image circles around the screen.


Tiles and UI layer
------------------

The tile and UI layers work exactly the same, except that:

 * The UI layer has a higher priority that the tile and sprite layer;
 * The tilemap size is twice as big as the UImap size.

The tile layer is usually used to display fixed game elements (like terrain), while the UI layer
is usually used to display textual elements (score, etc).

The tile (and UI) layer are composed of two parts: a __tileset__ and a __tilemap__. The tileset is a
collection of 16x128 tiles, 16x16 in size. The tilemap is a 80x60 (60x40 for the UI) grid where the tiles are placed.

The tileset and UIset are pointed, respectively, by `VIDTSET` and `VIDUSET`. The tilemap and UImap are pointed by 
`VIDTMAP` and `VIDUMAP`.

The tilemap have a displacement, just like the background (when double size is used). 
The displacement register is `VIDTMRL`.

If the tiles are larger than 8x8 (leaving empty spaces) or smaller than 8x8 (cutting the extra pixels), 
the `VIDSTSZ` register can be used to register this change in size. The structure is:

```c
struct VIDSTSZ {
    int8_t tile_x_sz : 4;
    int8_t tile_y_sz : 4;
    int8_t ui_x_sz   : 4;
    int8_t ui_y_sz   : 4;
}
```

Sprite layer
------------

The sprite layer, just like the tile layer, contain a __spriteset__ and a __spritemap__.

The __spriteset__ works just like the tilemap, the difference being that multiple sprites can be
grouped together to form a big sprite. The spriteset is pointed to by `VIDSPSET`.

The __spritemap__ is a list of 64-bit structs that contain sprite information. The list is pointed
to by `VIDSPMAP`. The number of sprites is defined by the register `VIDSPCNT`.

Each sprite is organized in the following structure:

```c
struct Sprite {
    uint16_t sprite_n;       /* sprite number in map (0xFFFF = don't show) */
    int16_t  scr_x;          /* X position in screen */
    int16_t  scr_y;          /* Y position in screen */
    uint8_t  priority : 4;   /* sprite priority */
    uint8_t  sz_x     : 4;   /* sprite horizontal size (in blocks of 8 px) */
    uint8_t  sz_y     : 4;   /* sprite vertical size (in blocks of 8 px) */
    bool     mirror_x;       /* mirror horizontally */
    bool     mirror_y;       /* mirror vertically */
    int      unused   : 2;
}
```

The whole screen can be scolled by the amounts defined in the register `VIDSCRL`.


Collisions
----------

A register `VIDCOL` detects collisions between two objects (tiles and sprites). The way the register
works is to add the index of each object to one 16-bit part of the 32-bit register, and read the
result. If the first bit is set to 1, there's a collision.

For tiles, the index is `(x + (y * 80)) | 0x8000`. For the sprite, simply use the sprite index.


Sound
=====

There are eight simultaneous sound channels, and the parameters for each sound channel are defined
in the registers `SNDCF[0..7]`. The configuration is defined by the following struct:

```c
struct SNDCF0 {
    uint16_t frequency;
    uint8_t  amplitude;  // volume
    uint8_t  waveform;
}
```

The waveform parameter can be one of:

 * 0 = square wave
 * 1 = sine wave
 * 2 = triangle wave
 * 3 = sawtooth wave
 * 4 = white noise
 * 5 = pink noise
 * 6 = custom wave

If a custom wave is defined, then a pointer to the 32-stage waveform needs to be present in `SNDCUS[0..7]`.

Additionally, an ADSR envelope (attack-delay-sustain-release) needs to be defined. It is defined in `SNDENV[0..7]`:

```c
struct SNDENV0 {
    uint8_t attack;
    uint8_t delay;
    uint8_t sustain;
    uint8_t release;
}
```

The audio configuration can be changed once by frame.


Timing & timers
===============

Due to the emulative characteristic of CoolVM, the timing happens a little different from other physical
computers, where everything happens at the same time.

In CoolVM, the devices operate sequencially. Every frame of 16.6 ms (60 Hz), the following happens:

 1. 16.384 CPU cycles are executed - during this time, timers and keyboard are avaliable;
 2. Audio is updated;
 3. Video is updated;
 4. If 16.6 ms haven't happened yet, the emulator is idle during this time.

This allow for very precise timing, whitin one timeframe.

The first bit of the register `TIMCFG` allows for changing this behaviour. If the bit is set to 1, the CPU will
execute as many CPU cycles as possible in one frame, and the audio/video are updated in a different thread. In
this configuration, the timer will lose meaning, as time will be host dependent. This mode is meant to be used
for very intense calculations.

If the frametime mode is used (`TIMCFG == 0`), the register `TIMFRM` contains the number of cycles still left
before the video/audio are updated.

The register `TIMWALL` contains the wall clock time (real time), containing the number of seconds since 
`2017-JAN-01 00:00:00`.

There are two reverse timers, with millisecond precision, called `TIMA` and `TIMB`. These clocks can be set by
software, and will run until when they reach zero, when the `ITIMA` and `ITIMB` interrupts are called, respectivelly.

When the video/audio finishes updating, the interrupt `ISCREEN` is called.

Inputs
======

There are three type of inputs in CoolVM: keyboard, joystick and mouse.

Keyboard
--------

Keyboard key presses and releases are stored in a queue. The front of the queue can be read on the register `KEYEVT`.
When the register is read, the value is lost and the queue moves to the next key event. This register can be read
multiple times in one single frame, and the format is:

```c
struct KEYEVT {
    uint8_t key;
    bool    special : 1;
    bool    pressed : 1;
    bool    shift   : 1;
    bool    control : 1;
    bool    alt     : 1;
}
```

If `KEYEVT` is zero, there are no events to read. If `special == 0`, then `key` is the ASCII code of the key pressed.
If not, then a special key was pressed (ESC, SHIFT, etc...).

When a new event is put into the queue, the interrupt `IKEY` is called.

Joystick
--------

CoolVM supports two joysticks, whose states can be read from the registers `JOY0` and `JOY1`. The format for these
registers is

```c
struct JOY0 {
    bool left       : 1;
    bool right      : 1;
    bool up         : 1;
    bool down       : 1;
    bool key_a      : 1;
    bool key_b      : 1;
    bool key_x      : 1;
    bool key_y      : 1;
    bool key_l      : 1;
    bool key_r      : 1;
    bool key_start  : 1;
    bool key_select : 1;
}
```

When the joystick state changes, the interrupt `IJOY` is called.

Mouse
-----

The mouse pointer is controlled by the host. From CoolVM, the mouse position can be read from `MOUPOS`. Each 16-bit
parts of the register contain the X and Y position. When the mouse moves, the `IMOUMOV` interrupt is called.

The state of the buttons of the mouse are stored in `MOUBTN`, with the following format:

```c
struct MOUBTN {
    bool left       : 1;
    bool middle     : 1;
    bool right      : 1;
    bool wheel_up   : 1;
    bool wheel_down : 1;
}
```

When a button is pressed, the `IMOUBTN` interrupt is called.

Disk access
===========

CoolVM supports up to 4 drives - which are usually mapped to files into the host. The drive configuration is 
defined in `DRVCFG`:

```c
struct DRVCFG {
    uint8_t drive_count;
    bool    drive0_readonly : 1;
    bool    drive1_readonly : 1;
    bool    drive2_readonly : 1;
    bool    drive3_readonly : 1;
    bool    drive0_ready    : 1;   // used for polling
    bool    drive1_ready    : 1;
    bool    drive2_ready    : 1;
    bool    drive3_ready    : 1;
}
```

Drives can be accessed through the registers `DRVDPOS`, `DRVMPOS`, `DRVSZ` and `DRVCMD`. 
`DRVDPOS` contains the position in the drive, `DRVMPOS` the position in the memory, and `DRVSZ` the size
of the operation, plus the first two bits defining the disk number. The operation is executed when the 
command is written in `DRVCMD`:

  * 0x0 = read from drive
  * 0x1 = write to drive

After the command is completed, the interrupt `IDRIVE[0..3]` is called.

Drives can have up to 4 Gb in size.

For a drive to be bootable, the drive must have the following magic number at the beginning: 
`01 DE 02 AD 03 BE 04 EF`, followed by the kernel position (32-bit) and the kernel size (32-bit). The
kernel will then be loaded with this size and position into memory position `0x8000`, and jumped to.


Assembler
=========

TODO
